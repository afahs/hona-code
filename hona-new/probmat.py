#!/usr/bin/python3

############################## LIB ##############################
import sys
sys.path.append('../commands')
import kube
import serf
import pickle
import numpy as np
import math
import statistics
import pandas as pd
import time
import timeit
##################################################################
def gettime(start,phrase):
	c=time.time() 
	print(phrase+": "+str(c-start))
	return c

def PickleLoad(filename):
	pickledlist=[]
	with open("/tmp/"+filename, 'rb') as f:
		pickledlist = pickle.load(f)
	return pickledlist


def GetLatency(Node1,Node2,latencies):
	for k,v in latencies[Node1]: 
		if k==Node2: 
			return v 
	print("latency not found, Returning 0")
	return 0

def GetSortedNodes(latencies): 
	Nodes=[]
	for k,v in latencies.items():
		Nodes.append(k)
	Nodes=sorted(Nodes)
	return Nodes

def GetLatMat(latencies,Nodes): 
	l=[]
	for node1 in Nodes:
		temp=[]
		for node2 in Nodes:
			temp.append(GetLatency(node1,node2,latencies))
		l.append(temp)
	LatMat=np.array(l)
	return LatMat

def CalculateF(x,B=1,index=1):
	assert x>0, "the latency value (%r) is negative or zero" % x
	if index==1: 
		return math.exp(-B*x)
	if index==2: 
		return 1/(B*x)
	if index==3:
		return 1/math.pow(x,B)

def GetProbMat(latencies,Nodes,B=1,index=1): 
	ProbMat=[]
	for node1 in Nodes: 
		temp=[]
		sumF=0
		for node2 in Nodes: 
			l=GetLatency(node1,node2,latencies)
			#Apply proxy-mity Routing function
			F=CalculateF(l,B=B,index=index)
			temp.append(F)
			sumF+=F 
		for node2 in Nodes: 
			temp[Nodes.index(node2)]=temp[Nodes.index(node2)]/sumF
		ProbMat.append(temp)
	ProbMat=np.array(ProbMat)
	return ProbMat


def MakeColRemove(ProbMat,Nodes,SelectedNodes):
	NIND=[]
	for i in range(len(Nodes)): 
		if Nodes[i] not in SelectedNodes: 
			NIND.append(i)
	temp=np.delete(ProbMat,NIND,axis=1)
	return temp

def CheckList(Nodes,SelectedNodes): 
	for node in SelectedNodes: 
		if node not in Nodes:
			print("Not Found"+node)
			return False
	return True

def CreateCaseMatrix(SelectedNodes,Nodes,ProbMat): # should be sorted Nodes and sorted SelectedNodes

	#start=time.time()
	assert CheckList(Nodes,SelectedNodes), "Not all of the selected nodes are found in the list of nodes"
	# Remove the probabilities of the nodes that does not have a server according to the tested placement.
	assert len(ProbMat)>0, "ProbMat is an empty Matrix"
	# An error occured in the calculation of ProbMat
	#start=gettime(start,"	Assert: ")
	# Remove the nodes that are not Selected from ProbMat
	Temp=MakeColRemove(ProbMat,Nodes,SelectedNodes)
	#start=gettime(start,"	MakeCol: ")
	# Normalize
	Temp /=  Temp.sum(axis=1)[:,np.newaxis]
	#start=gettime(start,"	Normalize: ")
	return Temp


def CreateListReq(Nodes,NodeObjects): # return the number of Received request for each node
	assert len(Nodes)==len(NodeObjects), "list of nodes is not equal to NodeObject" 
	#If this assert is issued that means that the nodes collected by Latencies(Serf) are different than those
	# of Node.pk (kubectl)=> serf have failed in a node or a node have failed. 
	ListReq=[-1 for i in range(len(Nodes))]
	TotalReq=0
	for node in NodeObjects:
		assert node.name in Nodes, node.name + " is Node available in Nodes"
		ListReq[Nodes.index(node.name)]=node.req
		TotalReq+=node.req
	return ListReq,TotalReq

def CalculateObjectiveVariables(ProbMat,LatMat,Nodes,ListReq,TotalReq,SelectedNodes,L): 
	Temp=[]
	#start=time.time()
	Temp= CreateCaseMatrix(SelectedNodes,Nodes,ProbMat) #ProbMat for the specific Placement
	#print("CreatMatrix Time: "+str(time.time()-start))
	slowReq	=	0	# Number of Slow Requests (Latency> L)
	RPP		=	[]	# Estimated Requests Per Pod 
	for i in range(len(SelectedNodes)):
		IND=Nodes.index(SelectedNodes[i])
		Total=0
		for j in range(len(Nodes)):
			assert Temp[j,i] > 0, "A probability is negative(%r), ProbMat is Wrong" % Temp[j,i]
			Req=Temp[j,i]*ListReq[j]
			Total+=Req
			if LatMat[j,IND] > L: 
				slowReq+=Req
		RPP.append(Total)
	Proximity= (1-(slowReq/TotalReq))*100 #in percent 
	std=statistics.stdev(RPP)
	Imbalance=(std/TotalReq)*100 # in percent
	#Calculating Imbalance Variable

	# for i in range(len(SelectedNodes)): 
	# 	IND=Nodes.index(SelectedNodes[i])
	# 	Total=0
	# 	for j in range(len(Nodes)):
	# 		assert Temp[j,i] > 0, "A probability is negative(%r), ProbMat is Wrong" % Temp[j,i]
	# 		Total+=Temp[j,i]*ListReq[j]
	# 	RPP.append(Total)
	# std=statistics.stdev(RPP)
	# Imbalance=(std/TotalReq)*100 # in percent
	return Proximity,Imbalance,Temp,RPP



