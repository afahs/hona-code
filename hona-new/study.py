#!/usr/bin/python3.5
import sys
sys.path.append('../commands')
import kube
import serf
import pickle
import math
import probmat as pm
import random
import copy
import numpy as np
import itertools
import pandas as pd
import operator
#Temp 
import time

class Case: 
	def __init__(self,Proximity,Imbalance,SelectedNodes,ProbMat,Number,RPP):
		self.id    		= Number
		self.Nodes 		= SelectedNodes
		self.Proximity	= Proximity
		self.Imbalance	= Imbalance
		self.RPP		= RPP
		self.Omega		= 0
def DfPrint(df):
	print(pd.DataFrame(df))	
def ComputeaOmega(Cases,alpha=0.95):
	Pmax=0
	Imin=100 
	for c in Cases: #compute Max value for Proximity and Min value for Imbalance
		if c.Proximity>Pmax: 
			Pmax=c.Proximity
		if c.Imbalance<Imin: 
			Imin=c.Imbalance
	for c in Cases: 
		c.Omega=alpha*(c.Proximity/Pmax)+(1-alpha)*(Imin/c.Imbalance)

def Testplacement(SelectedNodes,Nodes,ProbMat): 
	return

## Temp
def gettime(start,phrase): 
	print(time.time()-start,phrase)
	return time.time()

def StudyAllCases(ProbMat,LatMat,Nodes,ListReq,TotalReq,L,Replicas):
	cases=[]
	placements=itertools.combinations(Nodes,Replicas)
	for p in placements: 
		SelectedNodes=p
		Proximity,Imbalance,Temp,RPP=pm.CalculateObjectiveVariables(ProbMat,LatMat,Nodes,ListReq,TotalReq,SelectedNodes,L)
		cases.append(Case(Proximity,Imbalance,SelectedNodes,Temp,0,RPP))
		print(p,Proximity,Imbalance)
	return cases

def Study100Cases(ProbMat,LatMat,Nodes,ListReq,TotalReq,Replicas,L):
	cases=[]
	for i in range(100): 
		SelectedNodes=sorted(GetRandomSet(Nodes,Replicas))
		Proximity,Imbalance,Temp,RPP=pm.CalculateObjectiveVariables(ProbMat,LatMat,Nodes,ListReq,TotalReq,SelectedNodes,L)
		print(Proximity,Imbalance)
		cases.append(Case(Proximity,Imbalance,SelectedNodes,Temp,i+1,RPP))
	return cases

def CreateTestMat(LatMat,L):
	Lenlatmat=len(LatMat)
	temp=np.zeros([Lenlatmat,Lenlatmat], dtype=np.bool)
	for i in range(len(temp)): 
		for j in range(len(temp[0])):
			if LatMat[i,j]>=L: 
				temp[i,j]=False
			else: 
				temp[i,j]=True
	#print(pd.DataFrame(LatMat))
	#print(pd.DataFrame(temp))
	return temp


def TrimTestMat(TestMat,NIND): 
	temp=np.delete(TestMat,NIND,axis=1)
	temp=np.delete(temp,NIND,axis=0)
	return temp

def GetSecondLayerValues(Index,TestMat,y):# to Test How each not that is covered by placement Index is covered
	temp=[]
	for i in range(len(TestMat)): 
		if TestMat[Index,i]==True:
			temp.append(y[i])
	return sum(temp)

def GetMinReplicas(Nodes,LatMat,L):
	Allocate=[]
	TestMat=CreateTestMat(LatMat,L) # Create the Mat the indicates the fast link in the Network
	TNodes=np.array(Nodes) # Set of Gateways to be allocated a nearby replica
	#print(len(TestMat))
	while len(TNodes)!= 0:
		y=np.sum(TestMat,axis=1) # How many Fast link each node have
		ToBeAllocated=TNodes[0] # select the first node in the list (TBA)
		print("y",y)
		if y[0]==1: 
			Allocate.append(Nodes.index(TNodes[0]))
			TNodes=np.delete(TNodes,[0])
			TestMat=TrimTestMat(TestMat,[0])
			print("Allocated",Allocate)
			continue
		Possible={} # a Dict that describes how many fast link each node of TBA nearby Nodes have 
		Max=0
		for i in range(len(TestMat)): 
			if TestMat[0,i]==True:
				v=y[i]
				if v>Max: #select the Node\Nodes that have the highest number of Fast links
					Max=v
					Possible={}
					Possible[i]=v
				else:
					if v==Max: 
						Possible[i]=v
		if len(Possible)!=1:# if more than one Node have the same number of Fast links
			i=0
			Temp={}
			for k,v in Possible.items():
				v2=GetSecondLayerValues(k,TestMat,y) 
				if i==0: 
					Min=v2
					Temp[k]=(v,Min)
					i=1
				if v2<Min: # choose the one that contain badly placed node
					Temp={}
					Temp[k]=(v,v2)
			Possible=Temp
		k=list(Possible.keys())[0]
		print("Possible",Possible)
		assert len(Possible)==1, "The length of the Possible Solutions didn't reach one"
		Allocate.append(Nodes.index(TNodes[k]))
		NIND=[]
		for i in range(len(TestMat)): 
			if TestMat[k,i]==True:
				NIND.append(i)
		#print(TNodes)
		#print(pd.DataFrame(TestMat))
		TNodes=np.delete(TNodes,NIND)#Remove the nodes that got a nearby Node
		TestMat=TrimTestMat(TestMat,NIND)#Minimize the TestMat
		print("Allocated",Allocate)
		#print(TNodes)
		#print(pd.DataFrame(TestMat))
def GetPercentages(ListReq,TotalReq):
	assert TotalReq>0, "TotalReq=0"
	Weight=np.true_divide(ListReq,TotalReq)
	return Weight

def CheckPlacemnet(TestMat, SelectedNodes): 
	# Temp=TestMat[:,SelectedNodes] # find something faster DONE
	# y=np.sum(TestMat,axis=1)
	# print(y)
	for row in range(len(TestMat)): 
		Sum=0
		for i in SelectedNodes: 
			Sum+=TestMat[row,i]
		if Sum==0:
			return False
	return True

def TestSelectedNodes(Nodes,ProbMat,LatMat,TestMat,Servers,IsolatedNodes,Replicas,P,L): 
	assert len(IsolatedNodes)< Replicas, "The IsolatedNodes are greater than the number of replicas"
	print(Servers)
	SelectedNodes=[]
	if IsolatedNodes!=[]: 
		ToBeSelected=Replicas-len(IsolatedNodes)
	else: 
		ToBeSelected=Replicas
	i=0
	k=0
	Cases=[]
	while True:
		# Remove repetitive selected Nodes
		TempNodes=[]
		for node in IsolatedNodes: 
			TempNodes.append(node)
		if i==0: 
			for j in range(ToBeSelected): 
				TempNodes.append(Servers[j][0])

		else:
			Window=math.floor(i/5)+ToBeSelected +1
			if Window> Replicas*P: # change 
				Window=Replicas*P
			Set= [j for j,k in Servers[0:Window]]
			RandomSubet=random.sample(Set,ToBeSelected)
			for node in RandomSubet: 
				TempNodes.append(node)
		SelectedNodes=[Nodes[j] for j in sorted(TempNodes)]
		if CheckPlacemnet(TestMat, TempNodes)==True:
			k+=1 
			Proximity,Imbalance,Temp,RPP=pm.CalculateObjectiveVariables(ProbMat,LatMat,Nodes,ListReq,TotalReq,SelectedNodes,L)
			Cases.append(Case(Proximity,Imbalance,SelectedNodes,Temp,k,RPP))
		if k==100: 
			break
		i+=1

	for c in Cases: 
		print(c.id,c.Proximity,c.Imbalance,c.Nodes)
	best=GetBest(Cases)
	print(best.__dict__)

def GetBest(Cases, a=0.97):
	Min,Max=GetMinMax(Cases)
	for c in Cases:
		c.Omega=a*(c.Proximity/Max)+(1-a)*(Min/c.Imbalance)
	return GetBestD(Cases)

def GetBestD(Cases):
	best=Cases[0] 
	for c in Cases: 
		if c.Omega>best.Omega: 
			best=c
	return best

def GetMinMax(Cases):
	Max=Cases[0].Proximity
	Min=Cases[0].Imbalance 
	for c in Cases: 
		if c.Imbalance<Min:
			Min=c.Imbalance 
		if c.Proximity>Max:
			Max=c.Proximity
	return Min,Max

def GetPlacement(Nodes,ProbMat,LatMat,ListReq,TotalReq,Replicas,L):
	P=2
	Weight=GetPercentages(ListReq,TotalReq) # calculate weight per gateway
	Weight=Weight.round(2) #round the weight
	TestMat=CreateTestMat(LatMat,L) 
	DfPrint(TestMat)
	print(Weight)
	Temp = np.diag(Weight)
	TestMat=np.dot(Temp,TestMat) # Multiply the TestMat by the weight of each gateway
	DfPrint(TestMat)
	Weight=Weight[Weight!=0]
	y=np.sum(TestMat,axis=1)
	NIND=[]
	for i in range(len(y)): # remove gateway that does not produce load
		if y[i]==0: 
			NIND.append(i)
	TestMat=np.delete(TestMat,NIND,axis=0)
	y=np.delete(y,NIND)
	#TestMat=TestMat.round(2)
	#start=time.time()
	DfPrint(TestMat)
	Servers=[]
	IsolatedNodes=[]
	#print(Weight)
	for i in range(len(TestMat[0])): # find something faster
		v=sum(TestMat[:,i])
		if v!=0: 
			Servers.append((i,v))
	for i in range(len(y)): 
		if y[i]==Weight[i]: 
			IsolatedNodes.append(int(np.where(TestMat[i]!=0)[0]))
	print("Isolated Nodes:",end=" ")
	for i in IsolatedNodes: 
		print(Nodes[i],end=";")
	print("")
	Servers=sorted(Servers, key=lambda x: x[1],reverse=True)
	Servers=Servers[0:Replicas*P]
	TestSelectedNodes(Nodes,ProbMat,LatMat,TestMat,Servers,IsolatedNodes,Replicas,P,L)



def GetRandomSet(Nodes,n):
	names=[]
	#random.shuffle(Nodes)
	return random.sample(Nodes,n)

if __name__ == "__main__":
	# start=main=time.time()
	latencies=	pm.PickleLoad("lat.pk")
	# start=gettime(start,"getting latencies")
	Nodes=	pm.GetSortedNodes(latencies)
	# start=gettime(start,"geting sorted nodes ")
	NodeObjects=	pm.PickleLoad("node.pk")
	# start=gettime(start,"Getting nodes objects ")
	LatMat=		pm.GetLatMat(latencies,Nodes)
	# start=gettime(start, "creating LatMat")
	ProbMat=	pm.GetProbMat(latencies,Nodes,B=1,index=1)
	# start=gettime(start,"Creating ProbMat")
	ListReq,TotalReq=	pm.CreateListReq(Nodes,NodeObjects)

	GetPlacement(Nodes,ProbMat,LatMat,ListReq,TotalReq,5,20)
	#Study100Cases(ProbMat,LatMat,Nodes,ListReq,TotalReq,5,20)

	# start=time.time()
	# GetMinReplicas(Nodes,LatMat,15)
	# gettime(start,"get minRep")
	# start=time.time()
	# Cases=Study100Cases(ProbMat,LatMat,Nodes,ListReq,TotalReq,28,3)
	# print("All Cases Time: "+str(time.time()-start))
	# ComputeaOmega(Cases)
	# Cases.sort(key=lambda x: x.Omega, reverse=True)
	# for c in Cases: 
	# 	print(c.Proximity,c.Imbalance,c.Omega)




