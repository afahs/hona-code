#!/usr/bin/python3
import sys
sys.path.append('./commands')
sys.path.append('./deploy')
import kube
import scheduler as schd
import csv
from operator import itemgetter
from timeit import default_timer as timer

def check(variable,cases): 
	i=0
	for c in cases: 
		if c.res>variable: 
			i+=1
	print("Number of "+ str(variable)+" cases is: " +str(i)  )

def getmax(cases):
	maxc=cases[0]
	for c in cases: 
		if c.res>maxc.res: 
			maxc=c
	print("Max res:"+ str(maxc.res)+"max fair:" +str(maxc.fair))
	return c

def getmaxmin(cases): 
	max1=cases[0]
	min1=cases[0]
	for c in cases: 
		if max1.res<c.res: 
			max1=c
		if min1.Aver>c.Aver: 
			min1=c
	print("for max Res",max1.res,max1.Aver)
	print("for min Res",min1.res,min1.Aver)
	testfunc(max1,min1,"maxmin")

	return max1,min1

def getcase(Nodes,cases): 
 	for c in cases:
 		#print(c.nodes)
 		if (Nodes[0] in c.nodes) and (Nodes[1] in c.nodes) and (Nodes[2] in c.nodes) and (Nodes[3] in c.nodes) and (Nodes[4] in c.nodes):
 			return c


def ReloadDeps():
	Nodes=schd.PickleLoad("node.pk")
	Pods=schd.PickleLoad("pods.pk")
	Deps=schd.PickleLoad("deps.pk")
	Latencies=schd.PickleLoad("lat.pk")
	return Nodes,Pods,Deps,Latencies

def testfunc(case1,case2,name): 

	listoflate1=Getlatedist(case1)
	listoflate2=Getlatedist(case2)

	with open(name+".csv", 'w', newline='') as file:
		writer = csv.writer(file)
		writer.writerow(["Late","Prob","Cum","Type"])
		total=0
		for i in range(len(listoflate1)):
			total+=listoflate1[i][1]
			writer.writerow([listoflate1[i][0],listoflate1[i][1],total,"Tail"])
		total=0
		for i in range(len(listoflate2)):
			total+=listoflate2[i][1]
			writer.writerow([listoflate2[i][0],listoflate2[i][1],total,"Mean"])			

	return listoflate1,listoflate2


def Getlatedist(case): 
	distribution={}
	for k,v in case.prob.items(): 
		for k1,v1 in v.items():
			if v1[1] in distribution.keys(): 
				distribution[v1[1]]+=v1[0]
			else: 
				distribution[v1[1]]=v1[0]
	listoflate=[(k, v) for k, v in distribution.items()]
	listoflate.sort(key=itemgetter(0))
	return listoflate

def GetCum(case): 
	late=Getlatedist(case)
	total=0
	for i in range(len(late)):
		total+=late[i][1] 
		late[i]=(late[i][0],total)
	return late

def traceCumulative(cases,name="AllCases"): 

	with open(name+".csv", 'w', newline='') as file:
		writer = csv.writer(file)
		writer.writerow(["Late","Prob","Cum","Type"])
		for c in cases: 
			listoflate=Getlatedist(c)
			total=0
			for i in range(len(listoflate)):
				total+=listoflate[i][1]
				writer.writerow([listoflate[i][0],listoflate[i][1],total,"Case"+str(cases.index(c)+1)])

def Test(n=5,cases=5):
	Nodes,Pods,Deps,latencies=ReloadDeps()
	C1=schd.CreateVivaldiCases(Nodes,latencies,n,1,vres=99,vfair=5,vtime=5,vcount=cases-2,method="Mean",Change=1)
	C2=schd.CreateRandomCases(Nodes,latencies,n,vres=99,vfair=5,vtime=5,vcount=cases,method="Mean")
	traceCumulative(C1[0],name="AllCasesV")
	traceCumulative(C2[0],name="AllCasesR")
	for c in C1[0]: 
		print("V", c.res)
	for c in C2[0]: 
		print("R", c.res)

def CreateCDFFunction(case):
	Casefunction=[]
	late=GetCum(case)
	for i in range(len(late)): 
		if i==0: 
			(A,B,l1,l2)=GetEquation((0,0),late[i])
		else: 
			(A,B,l1,l2)=GetEquation(late[i-1],late[i])
		Casefunction.append((A,B,l1,l2))
	return Casefunction

def GetEquation(point1,point2): 
	A=(point2[1]-point1[1])/(point2[0]-point1[0])
	B=point2[1]-A*point2[0]
	return (A,B,point1[0],point2[0])	

def GetValuePoint(function,l): 
	for item in function:
		if item[2] <= l <= item[3]:
			return (item[0]*l+item[1])
	return  (item[0]*l+item[1])

def CalaulateAverage(Functions,point): 
	total=0 
	for f in Functions.items():
		function=f[1]
		total+=GetValuePoint(function,point)
	return total/len(Functions.items())


def GetAverageCDF(Cases):
	AverageCDF=[]
	points=[0,0.2]
	for i in range(1,51):
		points.append(i)
	Functions={}
	for case in Cases: 
		Functions[case]= CreateCDFFunction(case)
	for point in points: 
		AverageCDF.append((point,CalaulateAverage(Functions,point)))
	return AverageCDF

def TestAverage(n=5,cases=5,name="AverageCDF"):
	Nodes,Pods,Deps,latencies=ReloadDeps()
	C1=schd.CreateVivaldiCases(Nodes,latencies,n,0,vres=99,vfair=0,vtime=5,vcount=cases-2,method="Mean",Change=1)
	C2=schd.CreateRandomCases(Nodes,latencies,n,vres=99,vfair=0,vtime=5,vcount=2,method="Mean")
	print("len(Random)",len(C2[0]))
	print("len(Vivaldi)",len(C1[0]))
	ACDFR=GetAverageCDF(C2[0])
	ACDFV=GetAverageCDF(C1[0])
	with open(name+".csv", 'w', newline='') as file:
		writer = csv.writer(file)
		writer.writerow(["Late","Cum","Type"])
		for item in ACDFR: 
			writer.writerow([item[0],item[1],"R"])
		for item in ACDFV: 
			writer.writerow([item[0],item[1],"V"])

def newtestfunc(n=10,cases=100,name="meanvstail",nodes=4):
	Nodes,Pods,Deps,latencies=ReloadDeps() 
	maxs=[]
	mins=[]
	for i in range(n): 
		cases,timeout=schd.CreateRandomCases(Nodes,latencies,nodes,vres=99,vfair=0,vtime=5,vcount=2,method="Mean")
		max,min=getmaxmin(cases)
		# for c in cases: 
		# 	print(c.res,c.Aver)
		maxs.append(max)
		mins.append(min)
		print(len(cases))
		print("tail",max.res,max.Aver)
		print("mean",min.res,min.Aver)
		createCSV(maxs,mins,name)


def createCSV(MAX,MIN,name): 
	with open(name+".csv", 'w', newline='') as file:
		writer = csv.writer(file)
		writer.writerow(["Late","Prob","Cum","Type","ID"])
		#print(len(MAX))
		for j in range(len(MAX)): 
			listoflate1=Getlatedist(MAX[j])
			listoflate2=Getlatedist(MIN[j])
			total=0
			for i in range(len(listoflate1)):
				total+=listoflate1[i][1]
				writer.writerow([listoflate1[i][0],listoflate1[i][1],total,"Tail","Case"+str(j)])
			total=0
			for i in range(len(listoflate2)):
				total+=listoflate2[i][1]
				writer.writerow([listoflate2[i][0],listoflate2[i][1],total,"Mean","Case"+str(j)])		


def calculatetime(Replicas): 
	start = timer()
	schd.GetBest(Replicas,0,vres=99,vfair=5,vtime=5,vcount=20,method="Mean",algo="All",Change=1)
	mid   = timer()
	print("o",mid-start)
	start = timer()
	schd.GetBest(Replicas,1,vres=99,vfair=5,vtime=5,vcount=20,method="Mean",algo="Vivaldi",Change=1)
	mid   = timer()
	print("H2",mid-start)
	start = timer()
	schd.GetBest(Replicas,0,vres=99,vfair=5,vtime=5,vcount=20,method="Mean",algo="Vivaldi",Change=1)
	mid   = timer()
	print("H1",mid-start)
	start = timer()
	schd.GetBest(Replicas,0,vres=99,vfair=5,vtime=5,vcount=20,method="Mean",algo="Random",Change=1)
	mid   = timer()
	print("R",mid-start)


if __name__ == "__main__":
	schd.GetBest(10,0,vres=99,vfair=5,vtime=5,vcount=20,method="Mean",algo="Random",Change=1)

