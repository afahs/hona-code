#!/usr/bin/python3
import logging
LOG_FILENAME = 'kube.log'
logging.basicConfig(filename=LOG_FILENAME,level=logging.DEBUG, format='%(asctime)s %(levelname)s: %(message)s',filemode='w')
import subprocess as commands
import pprint
import os
from operator import itemgetter

import serf
import coordinates as co
import pickle
################################## GLOBAL VARIABLES ###################################

Nodes=[]
Pods=[]
Deps=[]

########################################## GET ########################################
def GetNodes():
	logging.info("Running: kubectl get nodes --show-labels")
	tup = commands.getstatusoutput("kubectl get nodes --show-labels")
	return tup[1]

def GetNodesLabels():
	logging.info("Running: kubectl get nodes --show-labels")
	tup = commands.getstatusoutput("kubectl get nodes --show-labels")
	return tup[1]

def GetNodeLabels(Node):
	logging.info("Running: kubectl get nodes"+Node+" --show-labels")
	tup = commands.getstatusoutput("kubectl get nodes "+Node+" --show-labels")
	return tup[1]

def GetarrayNodesLabels(Nodes):
	Node=""
	for i in range(len(Nodes)):
		Node=Node + " " + Nodes[i]
	logging.info("Running: kubectl get nodes "+Node+" --show-labels")
	tup = commands.getstatusoutput("kubectl get nodes "+Node+" --show-labels")
	return tup[1]

def GetPodsPerNode():
	logging.info("Running: kubectl get po -o wide")
	tup = commands.getstatusoutput("kubectl get po -o wide")
	return tup[1]

def GetDep():
	logging.info("Running: kubectl get deploy --show-labels -o wide")
	tup = commands.getstatusoutput("kubectl get deploy --show-labels -o wide")
	return tup[1]

def GetSvcs():
	logging.info("Running: kubectl get svc")
	tup = commands.getstatusoutput("kubectl get svc")
	return tup[1]
	

########################################## LABEL ######################################
def AddNodesLabel(Nodes,Label):
	if len(Nodes)>1:
		Node=' '.join(Nodes)
	else: 
		Node=Nodes[0]
	logging.info("Running: kubectl label nodes "+Node+" "+Label)
	print("Running: kubectl label nodes "+Node+" "+Label)
	tup = commands.getstatusoutput("kubectl label nodes "+Node+" "+Label)
	return tup

def DeleteNodesLabel(Nodes,Label):
	if len(Nodes)>1:
		Node=' '.join(Nodes)
	else: 
		Node=Nodes[0]
	logging.info("Running: kubectl label nodes "+Node+" "+Label+"-")
	print("Running: kubectl label nodes "+Node+" "+Label+"-")
	tup = commands.getstatusoutput("kubectl label nodes "+Node+" "+Label+"-")
	return tup

########################################## DEPLOYMENT #################################

def CreateDeployment(path):
	logging.info("Running: kubectl create -f "+path)
	tup = commands.getstatusoutput("kubectl create -f "+path)
	return tup

def DeleteDeployment(name):
	logging.info("Running: kubectl delete deployment "+name)
	tup = commands.getstatusoutput("kubectl delete deployment "+name)
	return tup

def ExposeDeployment(name):
	logging.info("Running: kubectl expose deployment "+name)
	tup = commands.getstatusoutput("kubectl expose deployment "+name)
	return tup
def DeleteSvc(name):
	logging.info("Running: kubectl delete svc "+name)
	tup = commands.getstatusoutput("kubectl delete svc "+name)
	return tup

def ScaleDeployment(name, number): 
	logging.info("Running: kubectl scale deployment.v1.apps/"+name+" --replicas="+str(number))
	tup = commands.getstatusoutput("kubectl scale deployment.v1.apps/"+name+" --replicas="+str(number))
	return tup
###################################################################################################


def GetAllNodes(master=True):
	out=GetNodes()
	Nodes=[]
	for x in out.splitlines():
		if ((" Ready" not in x) or ("NAME" in x) ): #remove failed nodes and first line
			if (master==False) and "master" in x: 
				continue
		else:
			x=(' '.join(x.split())).split()
			Nodes.append(x[0])
	return Nodes

def GetNumberPodsPerNode(Nodes):
	NumberOfPods={}
	for Node in Nodes: 
		NumberOfPods[Node]=0
	out=GetPodsPerNode()
	for x in out.splitlines():
		if "Running" in x: # check for running pods only
			NumberOfPods[x.split()[6]]+=1
	return NumberOfPods 

def ExtractNodeLabels(Node):
	out=GetNodeLabels(Node)
	for x in out.splitlines():
		if Node in x: 
			x=(' '.join(x.split())).split()[5].replace("/",",").split(",")
	return x

def ExtractAllNodesLabels():
	Nodes={}
	out=GetNodesLabels()
	for x in out.splitlines():
		if "hostname" in x:
			x=(' '.join(x.split())).split()
			key=x[0]
			value=x[5].replace("/",",").split(",")
			Nodes[key]=value
	return Nodes 

def ExtractLabelsLine(labels):
	r=[]
	labels=labels.split(",")
	for l in labels:
		if ("kubernetes.io" in l) or ("run" in l):
			r.append(l)
	for l in r:
		labels.remove(l)
	return labels

def LabelNodes(Nodes,label):
	return AddNodesLabel(Nodes,label)
	
def UnLabelNodes(Nodes,label):
	label=label.split("=")[0]
	return DeleteNodesLabel(Nodes,label)

def DeletePod(pod):
	logging.info("Running: kubectl delete po "+pod)
	tup = commands.getstatusoutput("kubectl delete po "+pod)
	return tup

################################## NODES Class and Functions ###########################

class node: 
	def __init__(self,line,master):
		line=line.split()
		self.name     =   line[0]  
		self.ip       =   serf.GetNodeIP(self.name)  					
		self.age      =   line[3] 					
		self.status   =   line[1]
		self.master   =   master
		self.labels	  =   ExtractLabelsLine(line[5])
		self.vec,self.adj,self.h       =   GetNodeCoordinates(self.name)
		self.pods     =   []
		self.req      =   0 
		
	def AddPods(self,Pods):
		for po in Pods: 
			if po.node == self.name: 
				self.pods.append(po)

	def AddLatancies(self,Latencies):
		self.latencies=   Latencies[self.name]

	def AddReq(self,Pods):
		self.req=CalculateRequestPerNode(Pods,self.name)

def CreateNodes():
	Nodes=[]
	out= GetNodes()
	for line in out.splitlines():
		if "NAME" not in line: # exlude the first line
			if "master" in line:
				Nodes.append(node(line,True))
			else:
				if " Ready"  in line:
					Nodes.append(node(line,False))
	return Nodes

def GetRequests(svc):
	Nodes=GetAllNodes()
	requestspernode=serf.ExtractLoad(svc,Nodes)
	return requestspernode

def GetNodeCoordinates(Node):
	out=serf.GetCoordinates(Node)
	out=out.splitlines()
	line=out[1].split("]")
	coor=line[0].replace("[","").split()
	#line=out[1].replace("[","").replace("]","").split()
	co=[]
	for x in coor:
		co.append(float(x))
	return co,float(line[1].split(",")[1]),float(line[1].split(",")[2])

def ExtractCoordinatesNodes(Nodes):
	co={}
	for node in Nodes: 
		co[node.name]=node.co
	return co
def ReturnMaster(Nodes):
	for node in Nodes: 
		if node.master==True:
			return node.name
################################## PODS Class and Functions ###########################

class pod: 
	def __init__(self,line,Deps):
		line=line.split()
		self.id    =   line[0] 					#ID of the pod in kube
		self.dep   =   GetDepName(line[0],Deps) # Deployment name
		self.ip    =   line[5] 					# pod Ip 
		self.node  =   line[6] 					# hosting node
		self.age   =   line[4] 					# age since creation
		self.res   =   line[3] 					# number of restarts
		for dep in Deps:
			if self.dep == dep.name:
				if dep.exposed==True:
					self.req=0
					self.exposed=True
					self.RPP = ComputeRequestsPerPod(self.ip,dep.RPN)
					for k,v in self.RPP:
						self.req+=v
				else:
					self.req=0
					self.exposed=False
					self.RPP=[]
def CreatePods(Deps):
	out= GetPodsPerNode()
	for line in out.splitlines():
		if "Running" in line:
			Pods.append(pod(line,Deps))
	return Pods 


def ComputeRequestsPerPod(podip,RPN):
	RPP=[]
	for k,v in RPN.items():
		for x in v:
			if podip in x: 
				RPP.append((k,x[1]))		
	return RPP

def GetDepName(string,Deps):
	for dep in Deps:
		if (dep.name+"-" in string) and ("-"+dep.name+"-" not in string):
			return dep.name 


################################## DEP Class and Functions ###########################

class dep:
	def __init__(self,line,svcs):
		line=line.split()

		self.name    =   line[0] 					# Deployment name
		self.age	 =   line[5] 	
		self.des   	 =   line[1] 					# Number of replicas Desired
		self.image   =   line[7] 					# Used image for the pods 
		self.labels  =   ExtractLabelsLine(line[9])
		self.pods    =   []
		self.exposed,self.svc = self.IsExposed(svcs)
		if self.exposed==True:
			self.RPN	 =   GetRequests(self.name)
		else:
			self.RPN     =   {}	

	def IsExposed(self,svcs):
		for line in svcs.splitlines(): 
			if self.name in line: 
				return True,line.split()[2]
		return False,"0.0.0.0"

	def AddPods(self,Pods):
		for po in Pods: 
			if po.dep ==self.name: 
				self.pods.append(po)
				
	def CalculateTotalRequests(self): 
		TotalRequests=0
		for pod in self.pods: 
			TotalRequests+=pod.req
		return TotalRequests

def CreateDeps():
	svcs=GetSvcs()
	out=GetDep()
	for line in out.splitlines():
		if ("NAME" not in line) and ("No resources found" not in line) :
			Deps.append(dep(line,svcs))
	return Deps

######################################## PRINT & CONDITIONS####################################

def IsIpv4(ip):
	x = ip.split('.')
	if len(x) != 4:
		return False
	for e in x : 
		if (not str.isdigit(e)):
			return False
		if (int(e) < 0) or (int(e) > 255):
			return False
	return True 

def PrintRequestsPerNode(requestspernode):
	for k,v in requestspernode.items():
		print(k)
		for x in v:
			print(x)

def PrintPods(Pods):
	if Pods!=[]:
		max_len_id=max([len(po.id) for po in Pods])
		if max_len_id <4 :
			max_len_id=4
		max_len_node=max([len(po.node) for po in Pods])
		if max_len_node <5:
			max_len_node=5
		max_len_dep=max([len(po.dep) for po in Pods])
		if max_len_dep <10:
			max_len_dep=10
		max_len_ip=max([len(po.ip) for po in Pods])
	else:
		max_len_id=4
		max_len_node=5
		max_len_dep=10
		max_len_ip=16
	padding=4
	print("{k:^{k_len}} {v1:^{dep}} {v2:^{ip}} {v3:^{node}} {v4:^8} {v5:^8} ".format(k_len=max_len_id+padding,dep=max_len_dep+padding,ip=max_len_ip+padding,node= max_len_node+padding,k="Pod", v1="Deployment", v2="IP", v3="Node",v4="#Req",v5="Exposed"))
	print("{k:^{k_len}} {v1:^{dep}} {v2:^{ip}} {v3:^{node}} {v4:^8} {v5:^8}".format(k_len=max_len_id+padding,dep=max_len_dep+padding,ip=max_len_ip+padding,node= max_len_node+padding,k=PrintDash(max_len_id), v1=PrintDash(max_len_dep), v2=PrintDash(max_len_ip), v3=PrintDash(max_len_node), v4=PrintDash(7),v5=PrintDash(7)))
	
	for po in Pods: 
		print("{k:^{k_len}} {v1:^{dep}} {v2:^{ip}} {v3:^{node}} {v4:^8} {v5:^8} ".format(k_len=max_len_id+padding,dep=max_len_dep+padding,ip=max_len_ip+padding,node= max_len_node+padding, k=po.id, v1=po.dep,  v2=po.ip, v3=po.node, v4=str(po.req) ,v5=str(po.exposed)))


def PrintNodes(Nodes):
	max_len_id=max([len(node.name) for node in Nodes])
	if max_len_id <4 :
		max_len_id=4
	max_len_ip=16
	labels=[]
	for node in Nodes:
		label=""
		first=True
		for l in node.labels: 
			if first==True:
				label=l
				first=False
			else:
				label=label+","+l
		labels.append(label)
		node.labels=label
	max_len_lab=max([len(l) for l in labels])
	if max_len_lab <6:
		max_len_lab = 6
	max_len_pods=7

	padding=4
	print("{k:^{k_len}} {v1:^{ip}} {v2:^{lab}} {v3:^{pods}} {v4:^8} {v5:^8}".format(k_len=max_len_id+padding,lab=max_len_lab+padding,ip=max_len_ip+padding,pods= max_len_pods+padding,k="Node", v1="IP", v2="Labels",v3="# Pods",v4="# Req",v5="Master"))
	print("{k:^{k_len}} {v1:^{ip}} {v2:^{lab}} {v3:^{pods}} {v4:^8} {v5:^8}".format(k_len=max_len_id+padding,lab=max_len_lab+padding,ip=max_len_ip+padding,pods= max_len_pods+padding,k=PrintDash(max_len_id), v1=PrintDash(max_len_ip), v2=PrintDash(max_len_lab), v3=PrintDash(max_len_pods), v4=PrintDash(8), v5=PrintDash(8)))

	for node in Nodes: 
		print("{k:^{k_len}} {v1:^{ip}} {v2:^{lab}} {v3:^{pods}} {v4:^8} {v5:^8}".format(k_len=max_len_id+padding,lab=max_len_lab+padding,ip=max_len_ip+padding,pods= max_len_pods+padding,k=node.name, v1=node.ip, v2=node.labels, v3=len(node.pods),v4=str(node.req), v5=str(node.master)))

def PrintDeps(Deps):
	if Deps!=[]:
		max_len_id=max([len(dep.name) for dep in Deps])
		if max_len_id <9 :
			max_len_id=9
		max_len_im=max([len(dep.image) for dep in Deps])
		if max_len_im<6:
			max_len_im=6
		max_len_ip=16
		max_len_des=8
	else: 
		max_len_id=9
		max_len_im=6
		max_len_ip=16
		max_len_des=8

	labels=[]
	for dep in Deps:
		label=""
		first=True
		for l in dep.labels: 
			if first==True:
				label=l
				first=False
			else:
				label=label+","+l
		labels.append(label)
		dep.labels=label
	if labels!=[]:		
		max_len_lab=max([len(l) for l in labels])
		if max_len_lab <8:
			max_len_lab = 8
	else:
		max_len_lab = 8

	padding=4
	print("{k:^{k_len}} {v1:^{ip}} {v2:^{lab}} {v3:^{des}} {v4:^{im}} {v5:^8} ".format(k_len=max_len_id+padding,lab=max_len_lab+padding,ip=max_len_ip+padding,des= max_len_des+padding,im=max_len_im+padding,k="Deployment", v1="ServiceIP", v2="Labels",v3="# Pods",v4="Image",v5="Exposed"))
	print("{k:^{k_len}} {v1:^{ip}} {v2:^{lab}} {v3:^{des}} {v4:^{im}} {v5:^8} ".format(k_len=max_len_id+padding,lab=max_len_lab+padding,ip=max_len_ip+padding,des= max_len_des+padding,im=max_len_im+padding,k=PrintDash(max_len_id), v1=PrintDash(max_len_ip), v2=PrintDash(max_len_lab), v3=PrintDash(max_len_des), v4=PrintDash(max_len_im), v5=PrintDash(8)))
	for dep in Deps: 
		print("{k:^{k_len}} {v1:^{ip}} {v2:^{lab}} {v3:^{des}} {v4:^{im}} {v5:^8} ".format(k_len=max_len_id+padding,lab=max_len_lab+padding,ip=max_len_ip+padding,des= max_len_des+padding,im=max_len_im+padding,k=dep.name, v1=dep.svc, v2=dep.labels,v3=dep.des,v4=dep.image,v5=str(dep.exposed)))
		

def PrintLoadsPods(Pods):
	if Pods!=[]:
		Nodes= [k for (k,v) in Pods[0].RPP]
	else:
		Nodes=[]
	Deps=[]
	for po in Pods:
		if po.dep not in Deps: 
			Deps.append(po.dep)
	if Deps!=[]:
		max_deps=max([len(d) for d in Deps])
		if max_deps < 10:
			max_deps=10
	else:
		max_deps=10
	if len(Nodes)>7: 
		linemax=7
	else: 
		linemax=len(Nodes)
	padding=1
	string1=CenterPrint(16,"POD") + CenterPrint(10,"Node") + CenterPrint(max_deps, "Deployment")+PrintDash(1,char="|")
	dashline=CenterPrint(16,PrintDash(10,char="-")) + CenterPrint(10,PrintDash(7,char="-")) + CenterPrint(max_deps,PrintDash(max_deps-2,char="-"))+PrintDash(1,char="|")
	strings=[]
	Nodes.sort()
	newPods=[]
	for node in Nodes:
		for po in Pods: 
			if po.node==node and po.exposed==True:
				newPods.append(po)
	Pods=newPods
	for i in range(linemax):
		string1=string1+PrintDash(padding,char=" ")+CenterPrint(len(Nodes[i])+2,Nodes[i])
		dashline=dashline+PrintDash(padding,char=" ")+CenterPrint(len(Nodes[i])+2,PrintDash(len(Nodes[i]),char="-"))
	string1=string1+PrintDash(padding,char="|")+CenterPrint(7,"Total")
	dashline=dashline+PrintDash(padding,char="|")+CenterPrint(7,PrintDash(5,char="-"))
	print(string1)
	print(dashline)
	for po in Pods:
		po.RPP.sort()
		values=[v for (k,v) in po.RPP ]
		string2=CenterPrint(16,po.ip)+ CenterPrint(10,po.node)+ CenterPrint(max_deps, po.dep)+PrintDash(1,char="|")
		for i in range(linemax):
			if values[i]==-1:
				string2=string2+PrintDash(padding,char=" ")+CenterPrint(len(Nodes[i])+2,"N/A")
			else:
				string2=string2+PrintDash(padding,char=" ")+CenterPrint(len(Nodes[i])+2,str(values[i]))
		T=0
		for (k,v) in po.RPP:
			T+=v
		string2=string2+PrintDash(padding,char="|")+CenterPrint(7,str(T))
		print(string2)
		strings.append(string2)
	Total={}
	for node in Nodes: 
		Total[node]=0
	for po in Pods: 
		for (k,v) in po.RPP:
			if v!=-1:
				Total[k]+=v
	string2=CenterPrint(16,"All")+ CenterPrint(10,"Ø")+ CenterPrint(max_deps,"All")+PrintDash(1,char="|")
	T=0
	for i in range(linemax):
		T+=Total[Nodes[i]]
		string2=string2+PrintDash(padding,char=" ")+CenterPrint(len(Nodes[i])+2,str(Total[Nodes[i]]))
	string2=string2+PrintDash(padding,char="|")+CenterPrint(7,str(T))
	print(dashline)
	print(string2)
	strings.append(string2)

def PrintLatencies(latencies):

	if latencies!={}:
		Nodes=[k for (k,v) in latencies.items()]
	else:
		Nodes=[]
	if Nodes!=[]:
		max_nodes=max([len(d) for d in Nodes])
		if max_nodes < 6:
			max_nodes=6
	else:
		max_nodes=6
	
	if len(Nodes)>9: 
		linemax=9
	else: 
		linemax=len(Nodes)

	padding=1
	string1=CenterPrint(max_nodes,"Node")
	dashline=CenterPrint(max_nodes,PrintDash(max_nodes-2,char="-"))
	strings=[]
	Nodes.sort()

	for i in range(linemax):
		string1=string1+PrintDash(padding,char=" ")+CenterPrint(len(Nodes[i])+2,Nodes[i])
		dashline=dashline+PrintDash(padding,char=" ")+CenterPrint(len(Nodes[i])+2,PrintDash(len(Nodes[i]),char="-"))
	print(string1)
	print(dashline)
	for node in Nodes:

		string2=CenterPrint(max_nodes, node)
		for node2 in Nodes:
			if Nodes.index(node2) > linemax-1: 
				break
			string2=string2+PrintDash(padding,char=" ")+CenterPrint(len(node2)+2,str(GetLat(node,node2,latencies)))
		print(string2)
		strings.append(string2)

def PrintDash(max_len,char="-"):
	string=""
	for i in range(max_len):
		string=string+char
	return string

def CenterPrint(max_len,value):
	pad=max_len-len(value)
	leftpad=int(pad/2)
	rightpad=pad-leftpad
	string=PrintDash(leftpad,char=" ")+value+PrintDash(rightpad,char=" ")
	return string
################################## Latencies #########################################

def CalculateLatencies(Nodes):
	latencies={}
	for node in Nodes:
		latencies[node.name]=[]
		for n in Nodes: 
			if n != node: 
				latencies[node.name].append((n.name,serf.SerfRtt(node.name,n.name)))
	for node in Nodes: 
		latencies[node.name].append((node.name,0.2))
	return latencies

def GetLat(Node1,Node2,latencies):
	for l in latencies[Node1]:
		if l[0]==Node2:
			return l[1]

def CalculateRequestPerNode(Pods,node):
	NumberOfRequests=0
	for pod in Pods: 
		for t in pod.RPP: 
			if t[0]==node:
				NumberOfRequests+=t[1]
	return NumberOfRequests

################################## Main Function #####################################

def Create():
	Deps=[]
	Pods=[]
	Nodes=[]
	labels=ExtractAllNodesLabels()
	pp = pprint.PrettyPrinter(indent=4)
	Deps=CreateDeps()
	Pods=CreatePods(Deps)
	Nodes=CreateNodes()
	Latencies=CalculateLatencies(Nodes)

	for node in Nodes: 
		node.AddPods(Pods)
		node.AddLatancies(Latencies)
		node.AddReq(Pods)

	for dep in Deps:
		dep.AddPods(Pods)
		CheckInitialLatencies(dep,Latencies)

	return Nodes,Pods,Deps


def CheckInitialLatencies(dep,Latencies):
	Path="/tmp/"+dep.name+".pk"
	if not os.path.exists(Path): 
		with open(Path, 'wb') as f:
			pickle.dump(Latencies, f, pickle.HIGHEST_PROTOCOL)



