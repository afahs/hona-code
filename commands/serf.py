#!/usr/bin/python3
import logging
LOG_FILENAME = 'serf.log'
logging.basicConfig(filename=LOG_FILENAME,level=logging.DEBUG, format='%(asctime)s %(levelname)s: %(message)s',filemode='w')
import subprocess as commands

######################################### Serf  ########################################

def GetCoordinates(Node):
	logging.info("Running: serf rtt "+Node)
	tup = commands.getstatusoutput("serf rtt "+Node)
	return tup[1]

def GetMembers():
	logging.info("Running: serf members")
	tup = commands.getstatusoutput("serf members")
	return tup[1]

def SerfQueryList(timeout,Nodes,svc,msg):
	Node=""
	for node in Nodes: 
		Node=Node + " -node="+node
	logging.info("Running: serf query -timeout="+str(timeout)+"s -no-ack "+Node+" "+svc+" "+msg)
	tup = commands.getstatusoutput("serf query -timeout="+str(timeout)+"s -no-ack "+Node+" "+svc+" "+msg)
	return tup[1]

def SerfQueryAll(timeout,svc,msg):
	logging.info("Running: serf query -timeout="+str(timeout)+"s -no-ack  "+svc+" "+msg)
	tup = commands.getstatusoutput("serf query -timeout="+str(timeout)+"s -no-ack "+svc+" "+msg)
	return tup[1]	

def SerfRtt(Node1,Node2):
	logging.info("Running: serf rtt "+Node1+" "+Node2)
	tup = commands.getstatusoutput("serf rtt "+Node1+" "+Node2 + " | grep Estimated")
	lat= float(tup[1].split(':')[1].split()[0])
	return lat

########################################################################################################

def ExtractMembers():
	Members=[]
	out=GetMembers()
	for x in out.splitlines():
		if "alive" in x: 
			Members.append(x.split()[0])
	return Members

def ExtractCoordinates(Node):
	Coordinates=[]
	out=GetCoordinates(Node)
	for x in out.splitlines():
		if "Estimated" not in x: 
			for y in x.replace("[","").replace("]","").split():
				Coordinates.append(float(y))
	return Coordinates

def ExtractAllCoordinates(): 
	Coordinates={}
	Nodes=ExtractMembers()
	print(Nodes)
	for Node in Nodes:
		Coordinates[Node]=ExtractCoordinates(Node)
	return Coordinates

def GetNodeIP(Node):
	out=GetMembers()
	for line in out.splitlines():
		if Node in line: 
			line=line.split()
			return line[3].split("=")[1]

def ExtractLoad(dep,Nodes):
	reply={}
	didnotrespond=[]
	out=SerfQueryAll(2,"svc",dep)
	out,cond=ChechResponse(Nodes,out,dep)
	if cond==False:
		out,cond=ChechResponse(Nodes,out,dep)
	for node in Nodes: 
		if node not in out: 
			#TO BE DONE add a flag + declare/check the node failure
			print(node+ "have failed")
	lines=out.splitlines()
	reply2=ExtractLoadOut(lines)
	Pods=ExtractPods(reply2)# TO BE DONE ADD a flag if no pods are returned
	for node in Nodes: # initiate the Load per Node structure
		reply[node]=[]
		for po in Pods: 
			reply[node].append((po,-1))

	if not CheckFixOut(lines):
		lines=FixOut(lines,dep)

	reply2=ExtractLoadOut(lines)

	for node in reply2.keys():
		for (po,val) in reply2[node]:
			index = -1
			target = po
			new_value = val
			for i, v in enumerate(reply[node]):
				if v[0] == target:
					index = i
					break
			if index >= 0:
				reply[node][index] = (reply[node][index][0], new_value)
	return reply


def ExtractLoadOut(lines2):
	lines=[]
	reply={}
	node=""
	for line in lines2:

		if ("dispatched" in line) or (line == ""):
			continue

		if "Response from" in line:
			if lines!=[]:
				reply[node]=ExtractLoadNode(node,lines)
			node=line.split()[2].replace("\'","").replace(":","")
			lines=[]
		
		else:
			lines.append(line)
	reply[node]=ExtractLoadNode(node,lines)
	return reply

def ExtractLoadNode(node,lines):
	load=[]
	for line in lines:
		if IsIpv4(line.split()[1]):
			load.append((line.split()[1],int(line.split()[0])))
	return load

def CheckPods(Pods,load):
	newload=[]
	for po in pods:
		found=False
		for k,v in load:
			if k==po:
				newload.append(k,v)
				found=True
				break
		if found==False:
			newload.append(po,-1)

	return newload

def CheckFixOut(lines):
	check=True
	for line in lines:
		if ("dispatched" in line) or (line == ""):
			continue
		if (("iptables:" in line) or ("Another app is currently holding" in line)) and ("Response" not in line):
			check=False
			break
	return check 


def FixOut(lines,dep):
	#TO BE DONE add a flag that this function was called
	print("fix is called")
	locked=[]
	failedproximity=[]
	for line in lines:
		if "Response from" in line:
			node=line.split()[2].replace("\'","").replace(":","")
		
		else:
			if ("iptables:" in line):
				failedproximity.append(node)
			if "app is currently holding" in line:
				locked.append(node)

	
	for line in lines: 
		if "Response from" in line: 
			node=line.split()[2].replace("\'","").replace(":","")
			if node in (locked or failedproximity):
				lines.remove(line)
		if ("iptables:" in line) and ("Response" not in line):
			lines.remove(line) 

	 
	out2=SerfQueryList(2,locked,"svc",dep)
	i=0
	while(CheckFixOut(out2)!=True and i<3):
		i+=1
		out2=SerfQueryList(2,locked,"svc",dep)
	#TO BE DONE add a flag for the unfixed nodes
	node=""
	stilllocked=[]
	lines2= out2.splitlines()
	for line in lines2:
		if "Response from" in line:
			node=line.split()[2].replace("\'","").replace(":","")
		
		if ("iptables:" in line) and ("Response" not in line):
				stilllocked.append(node)
	
	for line in lines2: 
		if "Response from" in line: 
			node=line.split()[2].replace("\'","").replace(":","")
			if node in stilllocked:
				lines2.remove(line)
		if (("iptables:" in line) and ("Response" not in line)) or ("dispatched" in line):
			lines2.remove(line)
	lines=lines+lines2
	return lines

def ChechResponse(Nodes,out,dep):
	didnotrespond=[]
	for node in Nodes:
		if node not in out: 
			didnotrespond.append(node)
	if didnotrespond !=[]:
		print(didnotrespond)
		out2=SerfQueryList(5,didnotrespond,"svc",dep)
		return out + out2,False
	return out,True


def ExtractPods(reply):
	Pods=[]
	for node,load in reply.items():
		for l in load:
			if l[0] not in Pods:
				Pods.append(l[0])
	return Pods

def ExtractPodsLoad(load):
	Pods=[]
	for l in load: 
		Pods.append(l[0])
	return Pods

def ReturnLoadPod(load,po):
	for l in load: 
		if l[0]==po:
			return l

def IsIpv4(ip):
	x = ip.split('.')
	if len(x) != 4:
		return False
	for e in x : 
		if (not str.isdigit(e)):
			return False
		if (int(e) < 0) or (int(e) > 255):
			return False
	return True 
