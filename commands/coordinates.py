#!/usr/bin/python3
import math
import sys
import serf
import kube




def CalculateCenteriod(Coor,dim,allnodes=True,Nodes=None):
	if allnodes==False and Nodes==None:
		return "no nodes are selected"
	if allnodes==True:
		Nodes=[] 
		for k,v in Coor.items():
			Nodes.append(k)
	ret= [0 for i in range(dim)]
	for Node in Nodes: 
		ret=CoordinatesOperation(ret,"+",vec2=Coor[Node])
	return CoordinatesOperation(ret,"*",factor=1/len(Nodes))


def CoordinatesOperation(vec1,Operation,vec2=None,factor=1):
	ret= [0 for i in range(len(vec1))]

	if Operation=="+" and vec2!=None:
		if len(vec1)!=len(vec2) and Operation!="*":
			return "falied, two vectors with different dim."
		for i in range(len(vec1)):
			ret[i]= vec1[i]+vec2[i]
		return ret
	
	if Operation=="-":
		if len(vec1)!=len(vec2) and Operation!="*":
			return "falied, two vectors with different dim."
		for i in range(len(vec1)):
			ret[i]= vec1[i]-vec2[i]
		return ret

	if Operation=="*":
		for i in range(len(vec1)):
			ret[i]= vec1[i]*factor
		return ret

def CalacuteMagnitude(vec): 
	s=0
	for x in vec: 
		s+=x*x
	return math.sqrt(s)

def CalculateRawDistance(vec1,vec2):
	return CalacuteMagnitude(CoordinatesOperation(vec1,"-",vec2=vec2))

def CalculateMaxRawDistance(Coor):
	centeroid=CalculateCenteriod(Coor,8)
	Nodes=[k for k in Coor.keys()]
	First=True
	for Node in Nodes:
		Distance=CalculateRawDistance(Coor[Node],centeroid)
		if First==True:
			First=False
			maxi=Distance
		else:
			if maxi < Distance:
				maxi=Distance
	return maxi

def CalculateADistance(n1,n2):
	dist=CalacuteMagnitude(CoordinatesOperation(n1.vec,"-",vec2=n2.vec)) +n1.h +n2.h
	ADistance= dist + n1.adj + n2.adj
	if ADistance>0:
		dist=ADistance
	return dist*1000

def CalculateTheNeigborhood(Coor,maxi,factor=1):
	Nodes=[k for k in Coor.keys()]
	NodesNeighborhood={}
	for Node in Nodes: 
		NodesNeighborhood[Node]=0
	for Node1 in Nodes: 
		for Node2 in Nodes:
			if Node2!=Node1:
				if CalculateRawDistance(Coor[Node1],Coor[Node2])<maxi*factor:
					NodesNeighborhood[Node1]+=1
	return NodesNeighborhood


def GetCoordinatesPortion(Coor,Nodes): 
	Portion={}
	for Node in Nodes:
		Portion[Node]=Coor[Node]
	return Portion

def PrintcCoordinates(Coor): 
	max_len=max([len(k) for k in Coor.keys()])
	padding=4
	print("{k:^{k_len}} {v}".format(k_len=max_len+padding, v="Coordinates", k="Node"))
	print("{k:^{k_len}} {v}".format(k_len=max_len+padding, v="------------------------------------------", k="-----------"))
	for k in sorted(Coor.keys()): 
		    print("{k:^{k_len}}  {v1:^22}  {v2:^22}  {v3:^22}  {v4:^22}  {v5:^22}  {v6:^22}  {v7:^22}  {v8:^22}".format(k_len=max_len+padding, v1=Coor[k][0],v2=Coor[k][1],v3=Coor[k][2],v4=Coor[k][3],v5=Coor[k][4],v6=Coor[k][5],v7=Coor[k][6],v8=Coor[k][7], k=k))