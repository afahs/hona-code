#!/usr/bin/python3

import sys
from importlib import reload
import time
import argparse
import pickle

sys.path.append('../commands')
sys.path.append('../replace')
import kube
import replace as re
def parseCliOptions():

		parser = argparse.ArgumentParser()

		parser.add_argument( '--Cycle', #imp
			dest       = 'Cycle', 
			type       = int,
			default    = 240,
			help       = 'Cycle time',
		)

		options        = parser.parse_args()
		return options.__dict__

def PickleNodes(): 
	Nodes,Pods,Deps=kube.Create()
	latencies=kube.CalculateLatencies(Nodes)
	with open('/tmp/node.pk', 'wb') as f:
		pickle.dump(Nodes,f, pickle.HIGHEST_PROTOCOL)
	with open('/tmp/pods.pk', 'wb') as f:
		pickle.dump(Pods,f, pickle.HIGHEST_PROTOCOL)
	with open('/tmp/deps.pk', 'wb') as f:
		pickle.dump(Deps,f, pickle.HIGHEST_PROTOCOL)
	with open('/tmp/lat.pk', 'wb') as f:
		pickle.dump(latencies,f, pickle.HIGHEST_PROTOCOL)


def WriteCsvRow(f,row):
	str1=""
	size=len(row)
	for i in range(size): 
		if i ==size-1: 
			str1+=row[i]+"\n"
		else:
			str1+=row[i]+","
	f.write(str1)


if __name__ == "__main__":
	options = parseCliOptions()
	f=open("test.csv","w")
	row=["Depname","Vres","Vfair","Cres","Cfair","Violation","Found","Node_sd","Total_load","time"]
	WriteCsvRow(f,row)
	f.close()
	t=options['Cycle']
	print("Cycle 0")
	print("______________________________________________________________________")
	PickleNodes()
	re.CheckDeps(99.5,5,1,1)
	i=0
	while True:
		reload(kube)
		print("Sleep mode for "+str(t) )
		i+=1
		print("Cycle "+str(i)) 
		print("______________________________________________________________________")
		print("")
		time.sleep(t)
		PickleNodes()
		re.CheckDeps(99.5,5,1,1)

