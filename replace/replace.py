#!/usr/bin/python3
import sys
import operator
import time
from statistics import stdev
import math
import pickle
sys.path.append('../commands')
sys.path.append('../deploy')
import kube
import scheduler as schd

def Replace(pod, node, dep, start,latencies): #node1 to be replaced
	with open("/tmp/"+dep.name+".pk", 'wb') as f:
		pickle.dump(latencies,f, pickle.HIGHEST_PROTOCOL)
	n=len(dep.pods)
	node1=[pod.node]
	label="app"+dep.name.split("-")[1]+"=hona"
	print("Started ADD",time.time()-start)
	print(kube.AddNodesLabel(node,label))
	print("Started DEL",time.time()-start)
	print(kube.DeleteNodesLabel(node1,label.split("=")[0]))
	time.sleep(5)
	print("Started UP",time.time()-start)
	print(kube.ScaleDeployment(dep.name,n+1))
	print("Started DeletePod",time.time()-start)
	print(kube.DeletePod(pod.id))
	print("Started Down",time.time()-start)
	print(kube.ScaleDeployment(dep.name, n))

def CheckDeps(Qres,Qfair,Tres,Tfair): 
	Nodes=schd.PickleLoad("node.pk")
	Pods=schd.PickleLoad("pods.pk")
	Deps=schd.PickleLoad("deps.pk")
	Latencies=schd.PickleLoad("lat.pk")

	values=schd.CalculateAverageInterNodeLatencies(Latencies)
	kube.PrintDeps(Deps)
	kube.PrintPods(Pods)
	RequestsPerNode=[]
	RPN={}
	for node in Nodes: 
		RequestsPerNode.append(node.req)
		RPN[node.name]=node.req
	print(RPN)
	for dep in Deps:
		if "hona=active" not in dep.labels:
			continue
		print("Checking Deploment: " + dep.name)
		InitLat=schd.PickleLoad(dep.name+".pk")
		start=time.time()
		#print(time.gmtime(start))
		#print("Started",start)
		TotalRequests=dep.CalculateTotalRequests()
		test=0
		for pod in dep.pods: 
			if pod.req<0: 
				test=1
				break
		if test==1: 
			continue
		Percentage=CalculatePercentage(dep,Nodes,TotalRequests)
		SelectedNodes=[]
		RequestsPerPod={}
		dist=[]
		Violation=False
		Sol=True
		solution=None
		for pod in dep.pods:
			SelectedNodes.append(pod.node)
			if TotalRequests!=0:
				RequestsPerPod[pod.node]=pod.req*100/TotalRequests
			else: 
				RequestsPerPod[pod.node]=100/len(dep.pods)
		print(RequestsPerPod)
		Case=case(Nodes,SelectedNodes,0,Latencies,InitLat,Percentage,values,method="Mean")
		for k,v in RequestsPerPod.items():
			dist.append(v)
		Case.fair=stdev(dist)
		print(dep.name+" % Req: " + str(Case.res)+" % std: "+ str(Case.fair))
		if (Case.res<Qres-Tres or Case.fair>Qfair+Tfair) and dep.CalculateTotalRequests()>300:
			Violation=True
			if Case.res<Qres-Tres:
				solution=FixPlacement(Case,dep,Deps,Nodes,Pods,Latencies,Qres,Qfair,"Res",method="Mean")
			else:
				solution=FixPlacement(Case,dep,Deps,Nodes,Pods,Latencies,Qres,Qfair,"fair",method="Mean")
		if solution!=None:
			Sol=True
			Replace(solution[1], [solution[0]], dep,start,Latencies)
			time.sleep(2)
		else:
			Sol=False
		#print("finished",time.time()-start)
		row=[dep.name,str(Qres),str(Qfair),str(Case.res),str(Case.fair),str(Violation),str(Sol),str(stdev(RequestsPerNode)),str(TotalRequests),time.strftime("%H:%M:%S", time.gmtime())]
		if dep.name=="hona-ali":
			f=open("test.csv","a")
			WriteCsvRow(f,row)
			f.close()

def CalculatePercentage(dep,Nodes,TotalRequests):
	Requests={}
	Percentage={}
	for node in Nodes: 
		Requests[node.name]=0
	for k,v in dep.RPN.items(): 
		for k1,v1 in v: 
			Requests[k]+=v1
	if TotalRequests==0:
		for k,v in Requests.items():
			Percentage[k]=1/len(Nodes)
	else:
		for k,v in Requests.items():
			Percentage[k]=v/TotalRequests
	return Percentage

def GetLat(Node1,Node2,latencies):
	for l in latencies[Node1]:
		if l[0]==Node2:
			return l[1]

def Extractnode(pods,NewNodes,SelectedNodes): 
	for node in NewNodes: 
		if node not in SelectedNodes: 
			newnode=node
	for node in SelectedNodes: 
		if node not in NewNodes:
			for pod in pods:
				if pod.node==node:
					ReplacePod=pod
	return newnode,ReplacePod

def GetMinMax(Cases):
	Min=Cases[0].fair
	Max=Cases[0].res 
	for c in Cases: 
		if c.fair<Min:
			Min=c.fair 
		if c.res>Max:
			Max=c.res
	return Min,Max

def GetBestD(Cases):
	best=Cases[0] 
	for c in Cases: 
		if c.d>best.d: 
			best=c
	return best

def mean(numbers):
    return float(sum(numbers)) / max(len(numbers), 2)


def WriteCsvRow(f,row):
	str1=""
	size=len(row)
	for i in range(size): 
		if i ==size-1: 
			str1+=row[i]+"\n"
		else:
			str1+=row[i]+","
	f.write(str1)

class case: 
	def __init__(self,Nodes,SelectedNodes,Number,InitLat,latencies,Percentage,values,method):

		self.id    =   Number
		self.nodes =   SelectedNodes
		prob  =   {}
		dist  =   {}
		Result=0
		for node in Nodes:
			prob[node.name]=schd.CalcProb(node.name,SelectedNodes,InitLat)
			for n in SelectedNodes:
				temp=[]
				temp=list(prob[node.name][n])
				temp[0]=temp[0]*Percentage[node.name]
				prob[node.name][n]=tuple(temp)
				if GetLat(node.name,n,latencies)< values[method]:
					Result+=prob[node.name][n][0]
		self.res=Result
		print(values[method])
		summ=0
		self.SlowSources={}
		for node in Nodes: 
			for node2 in SelectedNodes:
				x=GetLat(node.name,node2,latencies)
				if x>values[method] and prob[node.name][node2][0]>0.1:
					summ+=prob[node.name][node2][0]
					self.SlowSources[node]=self.SlowSources.get(node,0)+prob[node.name][node2][0]
					print("Source: "+node.name+ "  destinatation: "+node2 )
					print(x,prob[node.name][node2][1],prob[node.name][node2][0])
		print(summ)
		first=0
		for k,v in prob.items():
			if first==0: 
				for k1,v1 in v.items(): 
					dist[k1]=0
					first=1
			for k1,v1 in v.items(): 
				dist[k1]+=v1[0]
		dist1=[]
		for k,v in dist.items():
			dist1.append(v)
		self.fair = stdev(dist1)

def removerp(pods,rp): 
	newnodes=[]
	for pod in pods: 
		if pod !=rp: 
			newnodes.append(pod.node)
	return newnodes

def FixPlacement(Case,dep,Deps,Nodes,Pods,Latencies,Qres,Qfair,Reason,method="Mean"):
	a=0.97
	values=schd.CalculateAverageInterNodeLatencies(Latencies)
	TotalRequests=dep.CalculateTotalRequests()
	Percentage=CalculatePercentage(dep,Nodes,TotalRequests)
	slow={}
	PercentageSlow={}
	ReqperPod={}
	SelectedNodes=[]
	Cases=[]
	SubCases=[]
	solutions=[]
	for pod in dep.pods:
		slow[pod]=0
		for k,v in pod.RPP: 
			if GetLat(pod.node,k,Latencies)>values[method]: 
				slow[pod]+=v
	for pod in dep.pods:
		if pod.req==0: 
			PercentageSlow[pod]=0	 
		else:
			PercentageSlow[pod]=slow[pod]*100/pod.req
		ReqperPod[pod]=pod.req

	HotPods=[]
	SortedPods=[]
	NodesSubset=[]
	if Reason=="Res":
		PercentageSlow=sorted(PercentageSlow.items(), key=operator.itemgetter(1),reverse=True)
		for k,v in PercentageSlow:
			SortedPods.append(k)
		for Snode,v in Case.SlowSources.items():
			if Snode not in NodesSubset:
				NodesSubset.append(Snode) 
			for node in Nodes: 
				if GetLat(Snode.name,node.name,Latencies) < values[method]:
					if node not in NodesSubset: 
						NodesSubset.append(node)
	else:
		ReqperPod=sorted(ReqperPod.items(),key=operator.itemgetter(1))
		for k,v in ReqperPod:
			SortedPods.append(k)
		for i in range(int(0.25*len(ReqperPod))):
			HotPods.append(ReqperPod[len(ReqperPod)-i-1][0])
		for pod in HotPods: 
			for node in Nodes:
				if (GetLat(pod.node,node.name,Latencies) < values[method]) and (node not in NodesSubset):
					NodesSubset.append(node)

	Found=False				
	for pod in SortedPods:
		TRP=pod
		NWN=removerp(dep.pods,TRP)
		for node in NodesSubset:
			if (node.name not in NWN+[TRP.node]) and node.master!=True:
				c=schd.case(Nodes,NWN+[node.name],0,Latencies,Percentage,values,method="Mean")
				print(c.fair,c.res,"To Be Replaced",pod.node," Replaced by", node.name)
				SubCases.append(c)	
				if c.fair<Qfair and c.res>Qres:
					solutions.append(c) 
					Found=True
		if Found==False:
			Cases+=SubCases
		else:
			break

	if solutions==[]: # NO replacement that meet the QoS for this cycle(one pod replacement)
		Min,Max=GetMinMax(Cases+[Case])
		Case.d=a*(Case.res/Max)+(1-a)*(Min/Case.fair)
		for c in Cases:
			c.d=a*(c.res/Max)+(1-a)*(Min/c.fair)
			if c.d>Case.d: # is there a placement better than the current? 
				solutions.append(c)
		if len(solutions)!=0: # there is a better solution
			Min,Max=GetMinMax(solutions)
			for c in solutions: 
				c.d=a*(c.res/Max)+(1-a)*(Min/c.fair)
			solution=GetBestD(solutions)
		else:  #current solution is the best
			solution=None
	else: #there is a better placement that meet the requirements
		Min,Max=GetMinMax(solutions)
		for c in solutions: 
			c.d=a*(c.res/Max)+(1-a)*(Min/c.fair)
		solution=GetBestD(solutions)

	if solution!=None:
		for pod in dep.pods:
			SelectedNodes.append(pod.node)

		newnode,ReplacePod=Extractnode(dep.pods,solution.nodes,SelectedNodes)

		print("selected solution")
		print(solution.res,solution.fair)
		
		#print(SelectedNodes)
		#print(solution.nodes)

		print(SelectedNodes,ReplacePod.node,newnode)
		return newnode,ReplacePod

	else: 
		print("NO Solution")
		return None




	# if Reason=="Res": 
	# 	PercentageSlow = sorted(PercentageSlow.items(), key=operator.itemgetter(1))
	# 	ReplacePod=PercentageSlow[len(PercentageSlow)-1][0]
	# else:
	# 	ReqperPod=sorted(ReqperPod.items(),key=operator.itemgetter(1))
	# 	ReplacePod=ReqperPod[0][0]


	# for pod in dep.pods:
	# 	rp=pod
	# 	nwn=removerp(dep.pods,rp)
	# 	for node in Nodes: 
	# 		if (node.name not in nwn+[rp.node]) and node.master!=True:
	# 			c=schd.case(Nodes,nwn+[node.name],0,Latencies,Percentage,values,method="Mean")
	# 			Cases.append(c)

	# Min,Max=GetMinMax(Cases)
	# for c in Cases: 
	# 	c.d=a*(c.res/Max)+(1-a)*(Min/c.fair)
	# s=GetBestD(Cases)
	# print("new",s.res,s.fair)

	# Cases=[]
	# for pod in dep.pods:
	# 	if pod!=ReplacePod: 
	# 		SelectedNodes.append(pod.node)

	# for node in Nodes: 
	# 	if node.name not in SelectedNodes and node.master!=True: 
	# 		Cases.append(schd.case(Nodes,SelectedNodes+[node.name],0,Latencies,Percentage,values,method="Mean"))
	# Cases.sort(key=lambda x: x.res, reverse=True)
	# for c in Cases:
	# 	print(c.res,c.fair) 
	# 	if c.res>Qres and c.fair<Qfair: 
	# 		solutions.append(c)
	# print("solutions")
	# for c in solutions: 
	# 	print(c.res,c.fair)
	# if len(solutions)==0: # NO replacement that meet the QoS for this cycle(one pod replacement)
	# 	for c in Cases: 
	# 		if c.res>Case.res and c.fair<Case.fair: # is there a placement better than the current? 
	# 			solutions.append(c)
	# 	if len(solutions)!=0: # there is a better solution
	# 		Min,Max=GetMinMax(solutions)
	# 		for c in solutions: 
	# 			c.d=a*(c.res/Max)+(1-a)*(Min/c.fair)
	# 		solution=GetBestD(solutions)
	# 	else:  #current solution is the best
	# 		solution=None
	# else: #there is a better placement that meet the requirements
	# 	Min,Max=GetMinMax(solutions)
	# 	for c in solutions: 
	# 		c.d=a*(c.res/Max)+(1-a)*(Min/c.fair)
	# 	solution=GetBestD(solutions)
	# 	# if Reason=="Res": 
	# 	# 	solutions.sort(key=lambda x: x.res, reverse=True)
	# 	# 	solution=solutions[0]
	# 	# else: 
	# 	# 	solutions.sort(key=lambda x: x.fair)
	# 	# 	solution=solutions[0]
	# if solution!=None:
	# 	newnode=Extractnode(solution.nodes,SelectedNodes)
	# 	if newnode==ReplacePod.node:
	# 		print("NO Solution")
	# 		return None
	# 	print("selected solution")
	# 	print(solution.res,solution.fair)
		
	# 	#print(SelectedNodes)
	# 	#print(solution.nodes)

	# 	print(SelectedNodes,ReplacePod.node,newnode)
	# 	return newnode,ReplacePod

	# else: 
	# 	print("NO Solution")
	# 	return None
