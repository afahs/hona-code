#!/usr/bin/python3

import sys
import operator
sys.path.append('./commands')
import kube
import scheduler as schd

def WriteCsvRow(f,row):
	str1=""
	size=len(row)
	for i in range(size): 
		if i ==size-1: 
			str1+=row[i]+"\n"
		else:
			str1+=row[i]+","
	f.write(str1)

def CalculateAverage(Cases,res=1):
	sumres=0
	if res==1: 
		for c in Cases:
			sumres+=c.res
	else:
		for c in Cases:
			sumres+=c.fair
	return sumres/len(Cases)


def GetLat(Node1,Node2,latencies):
	for l in latencies[Node1]:
		if l[0]==Node2:
			return l[1]

def Extractnode(NewNodes,SelectedNodes): 
	for node in NewNodes: 
		if node not in SelectedNodes: 
			return node

def test():
	#test=[[3,97,10],[4,99,7],[5,99.5,5],[6,99.7,],[7,99.8,],[8,99.9],[9,99.95,],[10,99.97],[12,99.99],[15,99.999,]]
	vres=100
	vfair=0.2
	vtime=12
	iteraitions=100
	Number_of_Nodes=20
	nl=[3,4,5,6,7,8,9]
	f=open("testing.csv","w")
	row=["vres","vfair","Replica_Size","type","res","fair","Average_res","Average_fair","Number_of_Cases","Number_of_Nodes","timeout","iter"]
	WriteCsvRow(f,row)
	for j in range(len(nl)):
		n=nl[j]
		case,Cases,timeout=schd.GetBest(n,0,vres=vres,vfair=vfair,vtime=vtime,vcount=0,method="Mean",algo="All",Change=1)
		row=[str(vres),str(vfair),str(n),"A",str(case.res),str(case.fair),str(CalculateAverage(Cases,res=1)),str(CalculateAverage(Cases,res=0)),str(len(Cases)-1),str(Number_of_Nodes),str(timeout),str(0)]
		print(row)
		WriteCsvRow(f,row)
		for i in range(iteraitions):		
			case,Cases,timeout=schd.GetBest(n,0,vres=vres,vfair=vfair,vtime=vtime,vcount=0,method="Mean",algo="Vivaldi",Change=1)
			row=[str(vres),str(vfair),str(n),"V",str(case.res),str(case.fair),str(CalculateAverage(Cases,res=1)),str(CalculateAverage(Cases,res=0)),str(len(Cases)-1),str(Number_of_Nodes),str(timeout),str(i)]
			print(row)
			WriteCsvRow(f,row)

			case,Cases,timeout=schd.GetBest(n,1,vres=vres,vfair=vfair,vtime=vtime,vcount=0,method="Mean",algo="Vivaldi",Change=1)
			row=[str(vres),str(vfair),str(n),"V1",str(case.res),str(case.fair),str(CalculateAverage(Cases,res=1)),str(CalculateAverage(Cases,res=0)),str(len(Cases)-1),str(Number_of_Nodes),str(timeout),str(i)]
			print(row)
			WriteCsvRow(f,row)

			case,Cases,timeout=schd.GetBest(n,0,vres=vres,vfair=vfair,vtime=vtime,vcount=0,method="Mean",algo="Random",Change=1)
			row=[str(vres),str(vfair),str(n),"R",str(case.res),str(case.fair),str(CalculateAverage(Cases,res=1)),str(CalculateAverage(Cases,res=0)),str(len(Cases)),str(Number_of_Nodes),str(timeout),str(i)]
			print(row)
			WriteCsvRow(f,row)
			case,Cases,timeout=schd.GetBest(n,1,vres=vres,vfair=vfair,vtime=vtime,vcount=0,method="Mean",algo="Random",Change=1)
			row=[str(vres),str(vfair),str(n),"K",str(case.res),str(case.fair),str(CalculateAverage(Cases,res=1)),str(CalculateAverage(Cases,res=0)),str(len(Cases)),str(Number_of_Nodes),str(timeout),str(i)]
			print(row)
			WriteCsvRow(f,row)
	f.close()


def test2():
	vres=100
	vfair=0.2
	vtime=12
	Number_of_Nodes=20
	nl=[3,5,7,9]
	#nl=[3]
	f=open("test2.csv","w")
	#row=["vres","vfair","Replica_Size","type","res","fair","Average_res","Average_fair","Number_of_Cases","Number_of_Nodes","timeout","iter"]
	row=["Replica_Size","type","res","fair","Number_of_Cases"]
	WriteCsvRow(f,row)
	for j in range(len(nl)):
		n=nl[j]	
		case,Cases,timeout=schd.GetBest(n,0,vres=vres,vfair=vfair,vtime=vtime,vcount=0,method="Mean",algo="Random",Change=1)
		for c in Cases: 
			row=[str(n),"R",str(c.res),str(c.fair),str(Cases.index(c))]
			print(row)
			WriteCsvRow(f,row)

		case,Cases,timeout=schd.GetBest(n,0,vres=vres,vfair=vfair,vtime=vtime,vcount=0,method="Mean",algo="Vivaldi",Change=1)
		print(Cases)
		for c in Cases: 
			row=[str(n),"V",str(c.res),str(c.fair),str(Cases.index(c))]
			print(row)
			WriteCsvRow(f,row)
		case,Cases,timeout=schd.GetBest(n,1,vres=vres,vfair=vfair,vtime=vtime,vcount=0,method="Mean",algo="Vivaldi",Change=1)
		print(Cases)
		for c in Cases: 
			row=[str(n),"V1",str(c.res),str(c.fair),str(Cases.index(c))]
			print(row)
			WriteCsvRow(f,row)
		#row=[str(vres),str(vfair),str(n),"V",str(case.res),str(case.fair),str(CalculateAverage(Cases,res=1)),str(CalculateAverage(Cases,res=0)),str(len(Cases)-1),str(Number_of_Nodes),str(timeout),str(i)]
		#print(row)
		#WriteCsvRow(f,row)

		#	case,Cases,timeout=schd.GetBest(n,1,vres=vres,vfair=vfair,vtime=vtime,vcount=0,method="Mean",algo="Vivaldi",Change=1)
		#	row=[str(vres),str(vfair),str(n),"V1",str(case.res),str(case.fair),str(CalculateAverage(Cases,res=1)),str(CalculateAverage(Cases,res=0)),str(len(Cases)-1),str(Number_of_Nodes),str(timeout),str(i)]
		#	print(row)
		#	WriteCsvRow(f,row)

		#	case,Cases,timeout=schd.GetBest(n,0,vres=vres,vfair=vfair,vtime=vtime,vcount=0,method="Mean",algo="Random",Change=1)
		#	row=[str(vres),str(vfair),str(n),"R",str(case.res),str(case.fair),str(CalculateAverage(Cases,res=1)),str(CalculateAverage(Cases,res=0)),str(len(Cases)),str(Number_of_Nodes),str(timeout),str(i)]
		#	print(row)
		#	WriteCsvRow(f,row)
		#	case,Cases,timeout=schd.GetBest(n,1,vres=vres,vfair=vfair,vtime=vtime,vcount=0,method="Mean",algo="Random",Change=1)
		#	row=[str(vres),str(vfair),str(n),"K",str(case.res),str(case.fair),str(CalculateAverage(Cases,res=1)),str(CalculateAverage(Cases,res=0)),str(len(Cases)),str(Number_of_Nodes),str(timeout),str(i)]
		#	print(row)
		#	WriteCsvRow(f,row)
	f.close()