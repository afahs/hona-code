#!/usr/bin/python3
import itertools
import sys
import random
import math
import time
import datetime
from statistics import stdev
import pickle
sys.path.append('../commands')

import kube
import serf
import deployment_files as deploy
import coordinates as co

def Schedule(Replicas):
	case,cases,timeout=GetBest(Replicas,1,vres=99.5,vfair=4,vtime=10,vcount=20,method="Mean",algo="Vivaldi",Change=1)
	print(case.res,case.fair)
	return case.nodes

def Allocate(Nodes,Label):
	return kube.AddNodesLabel(Nodes,Label)

def Deploy(name="ali", image="alijawad/pwsip:ver1", label="app=lat", replicas=3, port=80,podlabel=None):
	deploy.CheckDir()
	Name_of_File=deploy.CreateDepFile(name=name, image=image, label=label, replicas=replicas, port=port,podlabel=podlabel)
	print(kube.CreateDeployment(Name_of_File)[1])
	return

def Expose(name):
		kube.ExposeDeployment(name)

def UnAllocate(Nodes,Label):
	return kube.DeleteNodesLabel(Nodes,Label)

def Run(name="ali", image="alijawad/pwsip:ver1", label="app=lat", replicas=3, port=80,podlabel=None,Nodes=None,latencies=None):
	label="app"+name+"=hona"
	SelectedNodes=Schedule(replicas)
	print(SelectedNodes)
	Allocate(SelectedNodes,label)
	Deploy(name=name, image=image, label=label, replicas=replicas, port=port,podlabel=podlabel)
	print(Expose("hona-"+name))
	Nodes=PickleLoad("node.pk")
	Init_lat=kube.CalculateLatencies(Nodes)
	Path="/tmp/hona-"+name+".pk"
	with open(Path, 'wb') as f:
		pickle.dump(Init_lat,f, pickle.HIGHEST_PROTOCOL)

def DeleteDeployment(name,label):
	Nodes=PickleLoad("node.pk")
	SelectedNodes=[]
	for node in Nodes: 
		SelectedNodes.append(node.name)
	print(SelectedNodes)
	print(kube.DeleteNodesLabel(SelectedNodes,label))
	print(kube.DeleteDeployment(name))
	print(kube.DeleteSvc(name))


############################ Calculate Probabilities  ################################

class case: 
	def __init__(self,Nodes,SelectedNodes,Number,latencies,Percentage,values,method):

		self.id    =   Number
		self.nodes =   SelectedNodes
		prob  =   {}
		dist  =   {}
		Result=0
		AverageLatency=0
		for node in Nodes:
			prob[node.name]=CalcProb(node.name,SelectedNodes,latencies)		
			for n in SelectedNodes:
				temp=[]
				temp=list(prob[node.name][n])
				temp[0]=temp[0]*Percentage[node.name]
				prob[node.name][n]=tuple(temp)
				if prob[node.name][n][1]< values[method]:
					Result+=prob[node.name][n][0]
				AverageLatency+=prob[node.name][n][0]*prob[node.name][n][1]
		self.res=Result
		self.Aver = AverageLatency/100
		self.prob=prob
		first=0
		for k,v in prob.items():
			if first==0: 
				for k1,v1 in v.items(): 
					dist[k1]=0
					first=1
			for k1,v1 in v.items(): 
				dist[k1]+=v1[0]
		dist1=[]
		for k,v in dist.items():
			dist1.append(v)
		self.fair = stdev(dist1)


def CreateCases(Nodes,latencies,n,method="Mean"):
	Cases=[]
	values=CalculateAverageInterNodeLatencies(latencies)
	TotalRequests=CalculateTotalRequests(Nodes)
	NewNodes=RemoveMaster(Nodes)
	Percentage=CalculatePercentage(Nodes,TotalRequests)
	cases=itertools.combinations(NewNodes,n)
	print("combination done")
	i=0
	for c in cases:
		if i>10000:
			break
		print(i)
		i+=1
		SelectedNodes=[n.name for n in c]
		Number=i
		Cases.append(case(Nodes,SelectedNodes,Number,latencies,Percentage,values,method))
	return Cases



def RemoveMaster(Nodes):
	NewNodes=[]
	for node in Nodes:
		if not node.master: 
			NewNodes.append(node)
	return NewNodes

def CalculateTotalRequests(Nodes):
	TotalRequests=0
	for node in Nodes:
		TotalRequests+=node.req
	return TotalRequests

def CalculatePercentage(Nodes,TotalRequests):
	Percentage={}
	if TotalRequests==0: 
		for node in Nodes: 
			Percentage[node.name]=1/len(Nodes)
	else:
		for node in Nodes: 
			Percentage[node.name]=node.req/TotalRequests
	return Percentage

def CalcProb(Node,SelectedNodes,latencies):
	lat=[]
	sumall  = 0
	podprob = []
	prob={}
	a=1
	b=1
	for node in SelectedNodes:
			lat.append(GetLat(node,Node,latencies))
	#print(lat)
	for l in lat:
		sumall+= math.exp(-b*l)
	for l in lat: 
		podprob.append(math.exp(-b*l)/sumall)
	for p in podprob:
		podprob[podprob.index(p)]=(a*p+(1-a)*(1/len(SelectedNodes)))*100
	#print("pod probability: "+ str(podprob))
	for i in range(len(SelectedNodes)):
		prob[SelectedNodes[i]]=(podprob[i],lat[i])
	return prob

def GetLat(Node1,Node2,latencies):
	for l in latencies[Node1]:
		if l[0]==Node2:
			return l[1]
def nCr(n,r):
    f = math.factorial
    return f(n) / f(r) / f(n-r)

def CalculateAverageInterNodeLatencies(latencies):
	values={}
	All=[]
	for (k,v) in latencies.items():
		for (k2,v2) in v:
			All.append(v2)
	All.sort()
	values["Mean"]   =  sum(All)/len(All)
	values["Median"]    =  All[int(len(All)/2)]
	return values


############################################# Node Weight Calculation ############################################

def GetRandomSet(Nodes,n):
	names=[]
	for node in Nodes: 
		names.append(node.name)
	random.shuffle(names)
	return random.sample(names,n)

def TestCondition(vres,vfair,vtime,start,res,fair,number):
	if number >100:
	  	print("time ended")
	  	return False,True	
	#if time.time()-start > vtime:
	#	print("time ended")
	#	return False,True
	if res>vres:
		if fair<vfair:
			return False,False

	return True,False

# def TestCondition(vres,vfair,vtime,start,res,fair):
# 	if time.time()-start > vtime:
# 		print("time ended")
# 		return False
# 	if res>vres:
# 		if fair<vfair:
# 			return False

# 	return True

def KNN(coor,n):
	Neighbourhoods={}
	Distance={}
	Nodes=sorted([k for k,v in coor.items()])
	Set=GetRandomSet(Nodes,n)
	for node in Set: 
		Neighbourhoods[node]=[]
		print("\n")
	for node in Nodes:
		Distance={}
		for S in Set: 
			Distance[S]=co.CalculateRawDistance(coor[S],coor[node])
		print(node,Distance)
		Neighbourhoods[min(Distance, key=Distance.get)].append(node)
		print("\n")
	print(Neighbourhoods)
	return(Neighbourhoods)

def CalaculateNodesWeight(Coor):
	weights={}
	Nodes=[k for k in Coor.keys()]
	maxi=co.CalculateMaxRawDistance(Coor)
	NodesNeighborhood =co.CalculateTheNeigborhood(Coor,maxi) #first measure for the weight
	NumberOfPods      =kube.GetNumberPodsPerNode(Nodes)   #second measure
	NumberOfTXRequests,NumberOfRXRequests,AverageLoad,NumberOfKubepackets  =GetLoad(Nodes)   # Third
	dicts=[NodesNeighborhood,NumberOfPods,NumberOfTXRequests,NumberOfRXRequests,AverageLoad,NumberOfKubepackets]
	big_dict = {}
	for k in Nodes:
		big_dict[k] = [d[k] for d in dicts]
	PrintBigDict(big_dict)
	#### Calculate the total number of requests
	TotalRequests= Totalof(NumberOfRXRequests)
	print(TotalRequests)

	#### Calculate the total number of requests
	TotalLoad=0
	for k,v in AverageLoad.items(): 
		TotalLoad+=1/float(v) 
	print(TotalLoad)

	#### Calculate the total number of requests
	TotalNumberOfNodes= len(Nodes) 
	print(TotalNumberOfNodes)

	for Node in Nodes: 
		#### calculate the weight of the neighbourhood
		WN = NodesNeighborhood[Node]/TotalNumberOfNodes
		#### calculate the weight of the Requests
		WR = int(NumberOfRXRequests[Node])/TotalRequests
		#### calculate the weight of the Load
		WL = float(AverageLoad[Node])/TotalLoad
		print(Node,WN,WR,WL)

def GetLoad(Nodes):
	NumberOfTXRequests={}
	NumberOfRXRequests={}
	AverageLoad={}
	NumberOfKubepackets={}
	for Node in Nodes:
		out=serf.SerfQuery(1,Node,"ali")
		Load=out.split(" ")
		NumberOfTXRequests[Node]=Load[6].split(":")[1]
		NumberOfRXRequests[Node]=Load[5].split(":")[1]
		AverageLoad[Node]=Load[3].split(":")[1]
		NumberOfKubepackets[Node]=Load[4].split(":")[1]
	print(NumberOfTXRequests,NumberOfRXRequests,AverageLoad,NumberOfKubepackets)
		
	return NumberOfTXRequests,NumberOfRXRequests,AverageLoad,NumberOfKubepackets

def Totalof(NumberOf): 
	total=0
	for k,v in NumberOf.items(): 
		total+=int(v) 
	return total

def PrintBigDict(big_dict):
	max_len=max([len(k) for k in big_dict.keys()])
	padding=6
	print(" {k:^{k_len}}  {v1:^15}  {v2:^15}  {v3:^15}      {v4:^15}    {v5:^15}    {v6:^15}".format(k_len=max_len+padding, v1="Nodes Neighbourhood", v2="Number Of Pods", v3="TX Requests", v4="RX Requests", v5="Average Load", v6="Kube Packets", k="Node"))
	print(" {k:^{k_len}}  {v1:^15}  {v2:^15}  {v3:^15}      {v4:^15}    {v5:^15}    {v6:^15}".format(k_len=max_len+padding, v1="-------------------", v2="--------------", v3="-----------", v4="-----------", v5="------------", v6="------------", k="-------------"))
	for k in sorted(big_dict.keys()): 
		    print(" {k:^{k_len}}    {v1:^15}   {v2:^15}    {v3:^15}    {v4:^15}    {v5:^15}    {v6:^15}".format(k_len=max_len+padding, v1=big_dict[k][0], v2=big_dict[k][1], v3=big_dict[k][2], v4=big_dict[k][3], v5=big_dict[k][4], v6=big_dict[k][5], k=k))	

############################################# Vivaldi based cases ############################################
def PickleLoad(filename):
	pickledlist=[]
	with open("/tmp/"+filename, 'rb') as f:
		pickledlist = pickle.load(f)
	return pickledlist

# class group:
# 	def __init__(self,nodes):
# 		self.nodes=nodes
# 		if not nodes[0].master:
# 			self.leader=nodes[0]
# 		else: 
# 			if len(nodes)>1:
# 					self.leader=nodes[1]
# 			else: 
# 				self.leader=None
# 		self.TotalRequests=0
# 		if self.leader!=None:
# 			for node in nodes:
# 				self.TotalRequests+=node.req
# 				if node.req>self.leader.req:
# 					if not node.master:
# 						self.leader=node
# 	def print(self):
# 		print(self.leader.name, self.TotalRequests)
# 		for node in self.nodes: 
# 			print(node.name,end=",")
# 		print("")


# def CreateVivaldiGroups(n,p1=2,p2=2):
# 	Nodes=PickleLoad("node.pk")
# 	latencies=PickleLoad("lat.pk")
# 	LatencyValues  = CalculateAverageInterNodeLatencies(latencies)
# 	TotalRequests  = CalculateTotalRequests(Nodes)
# 	AverageLoad    = TotalRequests/n
# 	NodesDensity   = int(len(Nodes)/(n*p1))
# 	if NodesDensity<2:
# 		if n*2<len(Nodes):
# 			NodesDensity=2
# 	TempNodes=PickleLoad("node.pk")
# 	Groups         = []
# 	while len(TempNodes) > 0:
# 		if len(TempNodes)<NodesDensity:
# 			Groups.append(group(TempNodes))
# 			break
# 		MainNode=random.choice(TempNodes)
# 		GroupNodes=[]
# 		LatenciesFromMain=[]
# 		for node in TempNodes:
# 			if node!=MainNode:
# 				LatenciesFromMain.append((co.CalculateADistance(MainNode,node),node))
# 		LatenciesFromMain.sort()
# 		GroupNodes.append(MainNode)
# 		TempNodes.remove(MainNode)
# 		for i in range(NodesDensity-1):
# 			node=LatenciesFromMain[i][1]
# 			GroupNodes.append(LatenciesFromMain[i][1])
# 			TempNodes.remove(node)
# 		Groups.append(group(GroupNodes))
# 	return Groups
class group:
	def __init__(self,nodes,value,Nodes,tech):
		self.nodes=nodes
		if tech==0:
			self.getleaderrequests(nodes)
		if tech==1:
			self.getleaderneighbors(nodes,Nodes,value)
	def print(self):
		print(self.leader.name, self.TotalRequests)
		for node in self.nodes: 
			print(node.name,end=",")
		print("")


	def getleaderrequests(self,nodes):
		if not nodes[0].master:
			self.leader=nodes[0]
		else: 
			if len(nodes)>1:
					self.leader=nodes[1]
			else: 
				self.leader=None
		self.TotalRequests=0
		if self.leader!=None:
			for node in nodes:
				self.TotalRequests+=node.req
				if node.req>self.leader.req:
					if not node.master:
						self.leader=node

	def getleaderneighbors(self,nodes,Nodes,value):
		Numberofneighbors={}
		self.TotalRequests=0
		for node in nodes:
			self.TotalRequests+=node.req
			Numberofneighbors[node]=0
		for node in nodes:
			if node.master==True: 
				continue
			for n in Nodes:
				if co.CalculateADistance(n,node) <value: 
					Numberofneighbors[node]+=1
		Numberofneighbors=sorted(Numberofneighbors.items(), key=lambda kv: kv[1], reverse=True)
		topnodes=[]
		first=0
		for node,neighbor in Numberofneighbors:
			if first==0: 
				topnodes.append((node,neighbor))
				first=1
			else: 
				if neighbor==topnodes[0][1]: 
					topnodes.append((node,neighbor))
				else:
					break
		if len(topnodes)==0: 
			self.leader=None
		else:
			if len(topnodes)==1:
				self.leader=topnodes[0][0]
			else:
				maxnode=topnodes[0][0]
				for t in topnodes:
					if maxnode.req<t[0].req: 
						maxnode=t[0]
				self.leader=maxnode  


def getmaxneighbors(Numberofneighbors):
 	if Numberofneighbors[0][0].master==False:
 		best=Numberofneighbors[0]
 	else:
 		if len(Numberofneighbors)>1:
 			best=Numberofneighbors[1]
 		else:
 			return None
 	for l in Numberofneighbors: 
 		if l[1]> best[1] and l[0].master==False:
 			best=l
 	return best

def CreateVivaldiGroups(Nodes,n,latencies,value,tech,p1=2,p2=2):
	LatencyValues  = CalculateAverageInterNodeLatencies(latencies)
	TotalRequests  = CalculateTotalRequests(Nodes)
	AverageLoad    = TotalRequests/n
	NodesDensity   = int(len(Nodes)/(n*p1))
	if NodesDensity<2:
		if n*2<len(Nodes):
			NodesDensity=2
	TempNodes=PickleLoad("node.pk")
	Groups         = []
	while len(TempNodes) > 0:
		if len(TempNodes)<NodesDensity:
			Groups.append(group(TempNodes,value,Nodes,tech))
			break
		MainNode=random.choice(TempNodes)
		GroupNodes=[]
		LatenciesFromMain=[]
		for node in TempNodes:
			if node!=MainNode:
				LatenciesFromMain.append((co.CalculateADistance(MainNode,node),node))
		LatenciesFromMain.sort()
		GroupNodes.append(MainNode)
		TempNodes.remove(MainNode)
		for i in range(NodesDensity-1):
			node=LatenciesFromMain[i][1]
			GroupNodes.append(LatenciesFromMain[i][1])
			TempNodes.remove(node)
		Groups.append(group(GroupNodes,value,Nodes,tech))
	return Groups

def GetLeaders(Groups):
	leaders=[]
	for g in Groups:
		if g.leader!=None:
			leaders.append(g.leader)
	return leaders

# def CreateVivaldiCases(Nodes,latencies,n,vres=99,vfair=5,vtime=5,vcount=20,method="Mean",Change=1):
# 	start = time.time()
# 	Groups=CreateVivaldiGroups(n,p1=2,p2=2)
# 	leaders=GetLeaders(Groups)
# 	values=CalculateAverageInterNodeLatencies(latencies)
# 	TotalRequests=CalculateTotalRequests(Nodes)
# 	Percentage=CalculatePercentage(Nodes,TotalRequests)
# 	#cases=itertools.combinations(leaders,n)
# 	i=0
# 	Cases=[]
# 	SelectedNodes=GetRandomSet(leaders,n)
# 	tempcase=case(Nodes,SelectedNodes,i,latencies,Percentage,values,method)
# 	print(tempcase.res,tempcase.fair,i)
# 	Cases.append(tempcase)
# 	i=1
# 	found = not TestCondition(vres,vfair,vtime,start,tempcase.res,tempcase.fair)
# 	while True:
# 		i+=1
# 		SelectedNodes=GetRandomSet(leaders,n) #GetRandomCase(leaders,n)
# 		Number=i
# 		tempcase=case(Nodes,SelectedNodes,Number,latencies,Percentage,values,method)
# 		print(tempcase.res,tempcase.fair,i)
# 		Cases.append(tempcase)
# 		if i%100==0: 
# 			if Change==1:
# 				print("change Group")
# 				Groups=CreateVivaldiGroups(Nodes,n,latencies,value,tech,p1=2,p2=2)
# 				leaders=GetLeaders(Groups)
# 		if found==False: 
# 			found = not TestCondition(vres,vfair,vtime,start,tempcase.res,tempcase.fair)
# 		if found==True and i>vcount: 
# 			break
# 	return Cases
def CreateVivaldiCases(Nodes,latencies,n,tech,vres=99,vfair=5,vtime=5,vcount=20,method="Mean",Change=1):
	start = time.time()
	values=CalculateAverageInterNodeLatencies(latencies)
	Groups=CreateVivaldiGroups(Nodes,n,latencies,values[method],tech,p1=1,p2=2)
	leaders=GetLeaders(Groups)
	TotalRequests=CalculateTotalRequests(Nodes)
	Percentage=CalculatePercentage(Nodes,TotalRequests)
	i=0
	Cases=[]
	SelectedNodes=GetRandomSet(leaders,n)
	tempcase=case(Nodes,SelectedNodes,i,latencies,Percentage,values,method)
	#print(tempcase.res,tempcase.fair,i)
	Cases.append(tempcase)
	i=1
	found,timeout =TestCondition(vres,vfair,vtime,start,tempcase.res,tempcase.fair,i)
	found= not found
	while True:
		i+=1
		SelectedNodes=GetRandomSet(leaders,n) #GetRandomCase(leaders,n)
		Number=i
		tempcase=case(Nodes,SelectedNodes,Number,latencies,Percentage,values,method)
		#print(tempcase.res,tempcase.fair,i)
		Cases.append(tempcase)
		if i%10==0: 
			if Change==1:
				#print("change Group")
				Groups=CreateVivaldiGroups(Nodes,n,latencies,values[method],tech,p1=1,p2=2)
				leaders=GetLeaders(Groups)
		if found==False: 
			found,timeout =TestCondition(vres,vfair,vtime,start,tempcase.res,tempcase.fair,i)
			found= not found
		if found==True and i>vcount: 
			break
	return Cases,timeout
############################################# Random based cases ############################################


# def CreateRandomCases(Nodes,latencies,n,vres=99,vfair=5,vtime=5,vcount=100,method="Mean"):
# 	start = time.time()
# 	i=0
# 	Cases=[]
# 	values=CalculateAverageInterNodeLatencies(latencies)
# 	TotalRequests=CalculateTotalRequests(Nodes)
# 	Percentage=CalculatePercentage(Nodes,TotalRequests)
# 	NewNodes=RemoveMaster(Nodes)
# 	SelectedNodes=GetRandomSet(NewNodes,n)
# 	tempcase=case(Nodes,SelectedNodes,0,latencies,Percentage,values,method)
# 	Cases.append(tempcase)
# 	print(tempcase.res,tempcase.fair,i)
# 	while TestCondition(vres,vfair,vtime,start,tempcase.res,tempcase.fair):
# 		i+=1
# 		SelectedNodes=GetRandomSet(NewNodes,n)
# 		#print(SelectedNodes)
# 		Number=i
# 		tempcase=case(Nodes,SelectedNodes,Number,latencies,Percentage,values,method)
# 		Cases.append(tempcase)
# 		print(tempcase.res,tempcase.fair,i)
# 	return Cases


# def GetBest(Replicas,vres=99,vfair=5,vtime=5,vcount=20,method="Mean",algo="Vivaldi",Change=1): 
# 	Nodes=PickleLoad("node.pk")
# 	latencies=PickleLoad("lat.pk")
# 	if nCr(len(Nodes), Replicas)<2000 or algo=="All": 
# 		Cases=CreateCases(Nodes,latencies,Replicas,method=method)
# 		Cases.sort(key=lambda x: x.res, reverse=True)
# 		TopOnePercent=[Cases[i] for i in range(int(len(Cases)/100))]
# 		TopOnePercent.sort(key=lambda x: x.fair, reverse=False)
# 		return TopOnePercent[0]
	
# 	else: 
# 		Top20=[]
# 		if algo=="Vivaldi":
# 			Cases=CreateVivaldiCases(Nodes,latencies,0,Replicas,vres=vres,vfair=vfair,vtime=vtime,vcount=vcount,method=method,Change=Change)
# 			Cases.sort(key=lambda x: x.res, reverse=True)
# 			for c in Cases: 
# 				if c.fair<vfair: 
# 					Top20.append(c)
# 			Top20.sort(key=lambda x: x.fair, reverse=False)
# 			return Top20[0]

# 		else:
# 			Cases=CreateRandomCases(Nodes,latencies,Replicas,vres=vres,vfair=vfair,vtime=vtime,vcount=vcount,method=method)
# 			Cases.sort(key=lambda x: x.res, reverse=True)
# 			for c in Cases: 
# 				if c.fair<vfair: 
# 					Top20.append(c)
# 			Top20.sort(key=lambda x: x.fair, reverse=False)
# 			return Top20[0]

def CreateRandomCases(Nodes,latencies,n,vres=99,vfair=5,vtime=5,vcount=100,method="Mean"):
	start = time.time()
	i=1
	Cases=[]
	values=CalculateAverageInterNodeLatencies(latencies)
	TotalRequests=CalculateTotalRequests(Nodes)
	Percentage=CalculatePercentage(Nodes,TotalRequests)
	NewNodes=RemoveMaster(Nodes)
	SelectedNodes=GetRandomSet(NewNodes,n)
	tempcase=case(Nodes,SelectedNodes,0,latencies,Percentage,values,method)
	Cases.append(tempcase)
	if vcount==1: 
		print(vcount)
		i=100
	#print(tempcase.res,tempcase.fair,i)
	found,timeout=TestCondition(vres,vfair,vtime,start,tempcase.res,tempcase.fair,i)
	while found:
		i+=1
		SelectedNodes=GetRandomSet(NewNodes,n)
		#print(SelectedNodes)
		Number=i
		tempcase=case(Nodes,SelectedNodes,Number,latencies,Percentage,values,method)
		Cases.append(tempcase)
		found,timeout=TestCondition(vres,vfair,vtime,start,tempcase.res,tempcase.fair,i)
		#print(tempcase.res,tempcase.fair,i)
	print(time.time()-start)
	return Cases,timeout	

def GetMinMax(Cases):
	Min=Cases[0].fair
	Max=Cases[0].res 
	for c in Cases: 
		if c.fair<Min:
			Min=c.fair 
		if c.res>Max:
			Max=c.res
	return Min,Max

def GetBestD(Cases):
	best=Cases[0] 
	for c in Cases: 
		if c.d>best.d: 
			best=c
	return best

def GetBest(Replicas,tech,vres=99,vfair=5,vtime=5,vcount=20,method="Mean",algo="Vivaldi",Change=1):
	a=0.97
	Nodes=PickleLoad("node.pk")
	latencies=PickleLoad("lat.pk")
	if algo=="All":
		Cases=CreateCases(Nodes,latencies,Replicas,method="Mean")
		Min,Max=GetMinMax(Cases)
		for c in Cases:
			c.d=a*(c.res/Max)+(1-a)*(Min/c.fair)
			if c.fair<vfair: 
				c.d+=0.5
			if c.res>vres: 
				c.d+=0.5
		best=GetBestD(Cases)
		return best,Cases,"False"

	if algo=="Vivaldi":
		Cases,timeout=CreateVivaldiCases(Nodes,latencies,Replicas,tech,vres=vres,vfair=vfair,vtime=vtime,vcount=vcount,method=method,Change=Change)
		Min,Max=GetMinMax(Cases)
		for c in Cases:
			c.d=a*(c.res/Max)+(1-a)*(Min/c.fair)
			if c.fair<vfair: 
				c.d+=0.5
			if c.res>vres: 
				c.d+=0.5
		best=GetBestD(Cases)
		return best,Cases,timeout

	if algo=="Random":
		if tech==1: 
			Cases,timeout=CreateRandomCases(Nodes,latencies,Replicas,vres=vres,vfair=vfair,vtime=vtime,vcount=1,method=method)
		else:
			Cases,timeout=CreateRandomCases(Nodes,latencies,Replicas,vres=vres,vfair=vfair,vtime=vtime,vcount=vcount,method=method)
		Min,Max=GetMinMax(Cases)
		for c in Cases:
			print(c.res,c.fair)
			c.d=a*(c.res/Max)+(1-a)*(Min/c.fair)
			if c.fair<vfair: 
				c.d+=0.5
			if c.res>vres: 
				c.d+=0.5
		best=GetBestD(Cases)
		return best,Cases,timeout
