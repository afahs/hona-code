import os
Path="/deployment_files"

def CreateDir():
	try:  
		os.mkdir(Path)
	except OSError as e:
		print(e)
		print ("Creation of the directory %s failed" % Path)
	else:  
		print ("Successfully created the directory %s " % Path)

def CheckDir():
	print("Saving deployment files to "+Path)
	if not os.path.isdir(Path):
		CreateDir()

def CreateDepFile(name="ali", image="alijawad/pwsip:ver1", label="app=lat", replicas=3, port=80,podlabel=None):
	if podlabel==None:
		podlabel="app="+name
	podlabelS=podlabel.split("=")
	label=label.split("=")
	key=label[0]
	value=label[1]
	f = open(Path +"/hona_"+name+".yaml", "w")
	f.write("apiVersion: apps/v1beta1\n")
	f.write("kind: Deployment\n")
	f.write("metadata:\n")
	f.write("  name: hona-"+name+"\n") #deployment name 
	f.write("  labels:\n")
	f.write("    hona: active\n")
	f.write("    "+podlabelS[0]+": "+podlabelS[1]+"\n") # deployment labels
	f.write("spec:\n")
	f.write("  replicas: "+str(replicas)+"\n") # number of pods
	f.write("  template:\n")
	f.write("    metadata:\n")
	f.write("      labels:\n")
	f.write("        "+podlabelS[0]+": "+podlabelS[1]+"\n") # pods label
	f.write("        hona: active\n")
	f.write("      annotations:\n")
	f.write("    spec:\n")
	f.write("      nodeSelector:\n")
	f.write("        "+key+": "+value+"\n")
	f.write("      tolerations:\n")
	f.write("      - key: "+key+"\n")
	f.write("        operator: Equal\n")
	f.write("        value: "+value+"\n")
	f.write("      containers:\n")
	f.write("      - name: hona-"+name+"\n")
	f.write("        image: "+image+"\n")
	f.write("        ports:\n")
	f.write("        - containerPort: "+str(port)+"\n")
	f.close() 
	Name_of_file=Path +"/hona_"+name+".yaml"
	print("The file "+Name_of_file +" is created")
	return str(Name_of_file)