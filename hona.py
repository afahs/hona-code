#!/usr/bin/python3
##################################### IMPORT ##############################################
import sys
sys.path.append('./commands')
sys.path.append('./deploy')
sys.path.append('./replace')
sys.path.append('./check')
sys.path.append('./hona-new')
##################################### LIBS ################################################
import argparse
import pprint

##################################### LOCAL LIBS ##########################################
import kube
import serf
import net
import deployment_files as dep
import scheduler as schd
import coordinates as co
import replace as re
import probmat as pm
from importlib import reload
import cases
##################################### PARSE ###############################################
def parseCliOptions():

		parser = argparse.ArgumentParser()

		parser.add_argument( '--command', #imp
			dest       = 'command', 
			type       = str,
			default    = 'Print',
			help       = 'commands to be passed: Print, Deploy, Check, Update',
		)

		options        = parser.parse_args()
		return options.__dict__


##################################### MAIN ###############################################
def GetOptions():
	options = parseCliOptions()
	return options

def Run(options):
	return

if __name__ == "__main__":
	options = parseCliOptions()
	
